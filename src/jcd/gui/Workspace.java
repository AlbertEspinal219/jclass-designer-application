package jcd.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.shape.Line;
import javafx.util.Callback;
import jcd.data.DataManager;
import jcd.controller.TopToolbarController;
import jcd.data.Argument;
import jcd.data.CheckBoxTableCelll;
import jcd.data.JcdClass;
import jcd.data.JcdClassPane;
import jcd.data.Method;
import jcd.data.Variable;
import saf.ui.AppGUI;
import saf.components.AppWorkspaceComponent;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.*;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_MAX_PANE = "max_pane";
    static final String CLASS_RENDER_CANVAS = "render_canvas";
    static final String CLASS_BUTTON = "button";
    static final String CLASS_EDIT_TOOLBAR = "edit_toolbar";
    static final String CLASS_EDIT_TOOLBAR_ROW = "edit_toolbar_row";
    static final String CLASS_COLOR_CHOOSER_PANE = "color_chooser_pane";
    static final String CLASS_COLOR_CHOOSER_CONTROL = "color_chooser_control";
    static final String EMPTY_TEXT = "";
    static final int BUTTON_TAG_WIDTH = 75;

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    //API stuff
    ArrayList<String> rtClasses;
    ArrayList<String> fxClasses;
    // Stuff
    boolean selectingState;
    boolean resizingState;
    boolean gridLinesRendered;
    Pane scrollPaneContent;
    ScrollPane scrollPane;
    BorderPane controlPane;
    BorderPane varMethodEditingPane;
    FlowPane variableEditingPane;
    FlowPane methodEditingPane;
    GridPane topEditingPane;
    GridPane gridLines;

    Label classNameLabel;
    Label packageLabel;
    Label parentLabel;
    Label interfaceLabel;
    Label externalParent;
    Label externalInterface;
    TextField classNameTA;
    TextField packageNameTA;
    TextField externalParentTA;
    TextField externalInterfaceTA;
    //ComboBox<String> parentChoice;
    MenuButton parentChoice;
    MenuButton interfaceChoice;
    Button variableAdd;
    Button variableDelete;
    Button methodAdd;
    Button methodDelete;

    Label variablesLabel;
    Label methodsLabel;

    TableView<Variable> variableTable;
    TableView<Method> methodTable;
    TableColumn<Variable, String> varName;
    TableColumn checkCol;

    ArrayList<TableColumn<Method, String>> columns;
    TableColumn<Method, String> methodName;
    TableColumn<Method, String> methodReturn;
    TableColumn<Method, Boolean> methodScope;
    TableColumn<Method, Boolean> methodAbstract;
    TableColumn<Method, String> methodAccess;

    TableColumn<Method, String> methodArg1;
    TableColumn<Method, String> methodArg2;
    TableColumn<Method, String> methodArg3;
    TableColumn<Method, String> methodArg4;
    TableColumn<Method, String> methodArg5;
    TableColumn<Method, String> methodArg6;
    TableColumn<Method, String> methodArg7;
    TableColumn<Method, String> methodArg8;
    TableColumn<Method, String> methodArg9;
    TableColumn<Method, String> methodArg10;

    //Array List of Table Columns???
    ArrayList<Button> buttons;
    private TopToolbarController TopToolbarController;

    public Workspace(AppTemplate initApp) throws IOException, InterruptedException {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        layoutGUI();
        setupHandlers();
    }

    public Workspace() {

    }

    private void layoutGUI() {

        scrollPaneContent = new Pane();
        controlPane = new BorderPane();
        controlPane.prefHeightProperty().bind(gui.getPrimaryScene().heightProperty().divide(2));
        varMethodEditingPane = new BorderPane();
        varMethodEditingPane.setPadding(new Insets(50, 10, 10, 10));
        variableEditingPane = new FlowPane();
        methodEditingPane = new FlowPane();

        variableEditingPane.prefHeightProperty().bind(controlPane.prefHeightProperty().divide(3));
        methodEditingPane.prefHeightProperty().bind(controlPane.prefHeightProperty().divide(2));

        varMethodEditingPane.setTop(variableEditingPane);
        varMethodEditingPane.setCenter(methodEditingPane);
        topEditingPane = new GridPane();
        topEditingPane.prefHeightProperty().bind(controlPane.prefHeightProperty().divide(1.5));
        topEditingPane.setPadding(new Insets(10, 10, 10, 10));
        scrollPane = new ScrollPane(scrollPaneContent);

        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        controlPane.setTop(topEditingPane);
        controlPane.setCenter(varMethodEditingPane);

        classNameLabel = new Label("Class Name:");
        packageLabel = new Label("Package:");
        parentLabel = new Label("Parent:");
        interfaceLabel = new Label("Interface:");

        classNameTA = new TextField();
        packageNameTA = new TextField();
        externalParentTA = new TextField();
        externalInterfaceTA = new TextField();
        parentChoice = new MenuButton();
        interfaceChoice = new MenuButton();
        parentChoice.setPrefWidth(200);
        interfaceChoice.setPrefWidth(200);

        externalParent = new Label("External Parent:");
        externalInterface = new Label("External Interface:");

        topEditingPane.add(classNameLabel, 0, 0);
        topEditingPane.add(packageLabel, 0, 1);
        topEditingPane.add(parentLabel, 0, 2);
        topEditingPane.add(externalParent, 0, 3);
        topEditingPane.add(interfaceLabel, 0, 4);
        topEditingPane.add(externalInterface, 0, 5);

        topEditingPane.add(classNameTA, 5, 0);
        topEditingPane.add(packageNameTA, 5, 1);
        topEditingPane.add(parentChoice, 5, 2);
        topEditingPane.add(externalParentTA, 5, 3);
        topEditingPane.add(interfaceChoice, 5, 4);
        topEditingPane.add(externalInterfaceTA, 5, 5);

        HBox varLabelPane = new HBox();
        variablesLabel = new Label("Variables: ");
        varLabelPane.getChildren().add(variablesLabel);
        varLabelPane.setAlignment(Pos.CENTER);
        varLabelPane.setSpacing(10);
        variableEditingPane.getChildren().add(varLabelPane);

        variableAdd = gui.initChildButton(varLabelPane, PLUS_ICON.toString(), EMPTY_TEXT, workspaceActivated);
        variableDelete = gui.initChildButton(varLabelPane, MINUS_ICON.toString(), EMPTY_TEXT, workspaceActivated);
        variableDelete.setAlignment(Pos.CENTER);

        variableTable = new TableView<>();
        variableTable.prefHeightProperty().bind(controlPane.prefHeightProperty().divide(3));
        variableTable.prefWidthProperty().bind(controlPane.prefWidthProperty().add(500));

        varName = new TableColumn("Name");
        varName.prefWidthProperty().bind(variableTable.widthProperty().divide(4));
        TableColumn<Variable, String> varType = new TableColumn("Type");
        varType.prefWidthProperty().bind(variableTable.widthProperty().divide(4));
        //TableColumn varScope = new TableColumn("Static");
        //varScope.prefWidthProperty().bind(variableTable.widthProperty().divide(4));
        TableColumn<Variable, String> varAccess = new TableColumn("Access");
        varAccess.prefWidthProperty().bind(variableTable.widthProperty().divide(4));

        checkCol = new TableColumn<>("Static");
        checkCol.prefWidthProperty().bind(variableTable.widthProperty().divide(4));
        checkCol.setCellValueFactory(new PropertyValueFactory("staticCbValue"));

        checkCol.setCellFactory(new Callback<TableColumn<Variable, Boolean>, TableCell<Variable, Boolean>>() {
            public TableCell<Variable, Boolean> call(TableColumn<Variable, Boolean> p) {

                return new CheckBoxTableCelll<Variable, Boolean>(app);

            }
        });

        //Callback<TableColumn<Variable, String>, 
        //TableCell<Variable, String>> cellFactoryV
        //  = (TableColumn<Variable, String> p) -> new EditingCellVariable(app);
        varName.setEditable(true);
        varType.setEditable(true);
        varAccess.setEditable(true);

        varName.setCellValueFactory(
                new PropertyValueFactory<>("pname"));
        varType.setCellValueFactory(
                new PropertyValueFactory<>("ptype"));
        varAccess.setCellValueFactory(
                new PropertyValueFactory<>("paccessibility"));

        varName.setCellFactory(TextFieldTableCell.forTableColumn());
        varName.setOnEditCommit(
                (CellEditEvent<Variable, String> t) -> {
                    DataManager dataManager = (DataManager) app.getDataComponent();
                    ((Variable) t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setPName(t.getNewValue());
                    dataManager.getselectedClass().setPaneText();
                });

        varType.setCellFactory(TextFieldTableCell.forTableColumn());
        varType.setOnEditCommit(
                (CellEditEvent<Variable, String> t) -> {
                    DataManager dataManager = (DataManager) app.getDataComponent();

                    String currentTableValue = t.getOldValue();

                    boolean previousLine = false;
                    for (String x : dataManager.getselectedClass().getClassPane().getAggregates()) {
                        if (x.equals(currentTableValue)) {
                            previousLine = true;
                        }
                    }
                    ((Variable) t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setPType(t.getNewValue());
                    dataManager.getselectedClass().setPaneText();

                    //Already an aggregation on this row? 
                    JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                    //Internal
                    boolean inDesign = false;
                    for (JcdClass x : dataManager.getClasses()) {
                        if (t.getNewValue().equals(x.getClassName())) {
                            inDesign = true;

                            if (previousLine) {
                                dataManager.getClassPanes().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                                selectedClassPane.getAggregateLines().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                                selectedClassPane.getAggregates().remove(currentTableValue);
                            }
                            Line aggregation = new Line();

                            if (dataManager.getselectedClass() != null) {
                                JcdClass selectedClass = dataManager.getselectedClass();
                                JcdClassPane selectedPane = selectedClass.getClassPane();
                                aggregation.startXProperty().bind(selectedPane.getAggregationConnecter().layoutXProperty().subtract(16));
                                aggregation.startYProperty().bind(selectedPane.getAggregationConnecter().layoutYProperty().add(20));

                                aggregation.endXProperty().bind(x.getClassPane().layoutXProperty());
                                aggregation.endYProperty().bind(x.getClassPane().getAggregationConnecter().layoutYProperty());

                                selectedPane.getAggregateLines().add(aggregation);
                                x.getClassPane().getAggregateLines().add(aggregation);

                                selectedPane.getAggregates().add(x.getClassName());

                                this.getDesignPane().getChildren().add(aggregation);

                                break;
                            }

                        }

                    }
                    if (previousLine) {
                        if (inDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                            dataManager.getClassPanes().remove(selectedClassPane.getAggregatePanes().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                            selectedClassPane.getAggregateLines().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                            selectedClassPane.getAggregatePanes().remove(selectedClassPane.getAggregatePanes().get(selectedClassPane.getAggregates().indexOf(currentTableValue)));
                            selectedClassPane.getAggregates().remove(currentTableValue);
                        }
                    }
                    if (inDesign == false) {
                        if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                            TopToolbarController.processAPIAggregate(t.getNewValue());
                        }
                    }

                });

        varAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        varAccess.setOnEditCommit(
                (CellEditEvent<Variable, String> t) -> {
                    DataManager dataManager = (DataManager) app.getDataComponent();
                    ((Variable) t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setPAccessibility(t.getNewValue());
                    dataManager.getselectedClass().setPaneText();
                });
        variableTable.setEditable(true);

        HBox varTablePane = new HBox();

        variableEditingPane.getChildren().add(varTablePane);
        varTablePane.getChildren().add(variableTable);

        variableTable.getColumns().addAll(varName, varType, checkCol, varAccess);

        HBox methodLabelPane = new HBox();
        methodsLabel = new Label("Methods: ");
        methodLabelPane.getChildren().add(methodsLabel);
        methodLabelPane.setAlignment(Pos.CENTER);
        methodLabelPane.setSpacing(10);
        methodEditingPane.getChildren().add(methodLabelPane);

        methodAdd = gui.initChildButton(methodLabelPane, PLUS_ICON.toString(), EMPTY_TEXT, workspaceActivated);
        methodDelete = gui.initChildButton(methodLabelPane, MINUS_ICON.toString(), EMPTY_TEXT, workspaceActivated);

        methodTable = new TableView();
        methodTable.prefHeightProperty().bind(controlPane.prefHeightProperty().divide(3));
        methodTable.prefWidthProperty().bind(controlPane.prefWidthProperty().add(550));

        methodName = new TableColumn("Name");
        methodReturn = new TableColumn("Return");
        methodScope = new TableColumn("Static");
        methodAbstract = new TableColumn("Abstract");
        methodAccess = new TableColumn("Access");
        methodArg1 = new TableColumn("Arg1");
        methodArg2 = new TableColumn("Arg2");

        methodArg3 = new TableColumn("Arg3");
        methodArg3.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg4 = new TableColumn("Arg4");
        methodArg4.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg5 = new TableColumn("Arg5");
        methodArg5.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg6 = new TableColumn("Arg6");
        methodArg6.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg7 = new TableColumn("Arg7");
        methodArg7.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg8 = new TableColumn("Arg8");
        methodArg8.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg9 = new TableColumn("Arg9");
        methodArg9.prefWidthProperty().bind(methodTable.widthProperty().divide(5));
        methodArg10 = new TableColumn("Arg10");
        methodArg10.prefWidthProperty().bind(methodTable.widthProperty().divide(5));

        columns = new ArrayList();
        columns.add(methodName);
        columns.add(methodReturn);
        columns.add(methodAccess);

        HBox methodTablePane = new HBox();

        methodName.setEditable(true);
        methodReturn.setEditable(true);
        methodScope.setEditable(true);
        methodAccess.setEditable(true);
        methodTable.setEditable(true);

        methodName.setCellValueFactory(
                new PropertyValueFactory<>("pname"));
        methodName.setCellFactory(TextFieldTableCell.forTableColumn());
        methodName.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setPName(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
            }
        }
        );

        methodReturn.setCellFactory(TextFieldTableCell.forTableColumn());
        methodReturn.setCellValueFactory(
                new PropertyValueFactory<>("preturnType"));
        methodReturn.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setPReturnType(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);

                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().getUsesConnector().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().getUsesConnector().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }
            }
        }
        );

        methodScope.setCellValueFactory(new PropertyValueFactory("pstaticMethod"));
        methodScope.setCellFactory(new Callback<TableColumn<Method, Boolean>, TableCell<Method, Boolean>>() {
            public TableCell<Method, Boolean> call(TableColumn<Method, Boolean> p) {

                return new CheckBoxTableCelll<Method, Boolean>(app);

            }
        });
        methodAbstract.setCellValueFactory(new PropertyValueFactory("pabstractMethod"));
        methodAbstract.setCellFactory(new Callback<TableColumn<Method, Boolean>, TableCell<Method, Boolean>>() {
            public TableCell<Method, Boolean> call(TableColumn<Method, Boolean> p) {

                return new CheckBoxTableCelll<Method, Boolean>(app);

            }
        });

        methodAccess.setCellValueFactory(
                new PropertyValueFactory<>("paccess"));
        methodAccess.setCellFactory(TextFieldTableCell.forTableColumn());
        methodAccess.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setPAccess(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
            }
        }
        );

        methodArg1.setEditable(true);
        methodArg1.setCellValueFactory(
                new PropertyValueFactory<>("parg1Type"));
        methodArg1.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg1.setOnEditCommit((CellEditEvent<Method, String> t) -> {
            DataManager dataManager = (DataManager) app.getDataComponent();
            String currentTableValue = t.getOldValue();

            boolean previousLine = false;
            for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                if (x.equals(currentTableValue)) {
                    previousLine = true;
                }
            }
            ((Method) t.getTableView().getItems().get(
                    t.getTablePosition().getRow())).setparg1TypeProperty(t.getNewValue());
            dataManager.getselectedClass().setPaneText();
            boolean inDesign = false;
            JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
            for (JcdClass x : dataManager.getClasses()) {
                if (t.getNewValue().equals(x.getClassName())) {
                    inDesign = true;
                    if (previousLine) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                    Line useLine = new Line();
                    useLine.getStrokeDashArray().addAll(2d, 11d);

                    if (dataManager.getselectedClass() != null) {
                        JcdClass selectedClass = dataManager.getselectedClass();
                        JcdClassPane selectedPane = selectedClass.getClassPane();
                        useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                        useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                        useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                        useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                        selectedPane.getUsesLines().add(useLine);
                        x.getClassPane().getUsesLines().add(useLine);

                        selectedPane.getUses().add(x.getClassName());

                        getDesignPane().getChildren().add(useLine);
                        break;
                    }

                }

            }
            if (previousLine) {
                if (inDesign == false) {
                    dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                    dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                    selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                    selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                    selectedClassPane.getUses().remove(currentTableValue);
                }
            }
            if (inDesign == false) {
                if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                    TopToolbarController.processAPIUses(t.getNewValue());
                }
            }
        });

        methodArg2.setCellValueFactory(
                new PropertyValueFactory<>("parg2Type"));
        methodArg2.setEditable(true);
        methodArg2.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg2.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg2TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg3.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }
            }
        }
        );

        methodArg3.setCellValueFactory(
                new PropertyValueFactory<>("parg3Type"));
        methodArg3.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg3.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg3TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg3.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg4.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);

                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg4.setCellValueFactory(
                new PropertyValueFactory<>("parg4Type"));
        methodArg4.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg4.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg4TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg4.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg5.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg5.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg5.setCellValueFactory(
                new PropertyValueFactory<>("parg5Type"));
        methodArg5.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg5TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg5.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg6.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg6.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg6.setCellValueFactory(
                new PropertyValueFactory<>("parg6Type"));
        methodArg6.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg6TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg6.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg7.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg7.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg7.setCellValueFactory(
                new PropertyValueFactory<>("parg7Type"));
        methodArg7.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg7TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg7.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg8.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg8.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg8.setCellValueFactory(
                new PropertyValueFactory<>("parg8Type"));
        methodArg8.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg8TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg8.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg9.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg9.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg9.setCellValueFactory(
                new PropertyValueFactory<>("parg9Type"));
        methodArg9.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg9TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg9.setVisible(false);
                }
                if (!t.getNewValue().equals("") && !t.getNewValue().equals(null)) {
                    methodArg10.setVisible(true);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }

            }
        }
        );
        methodArg10.setCellFactory(TextFieldTableCell.forTableColumn());
        methodArg10.setCellValueFactory(
                new PropertyValueFactory<>("parg10Type"));
        methodArg10.setOnEditCommit(
                new EventHandler<CellEditEvent<Method, String>>() {
            @Override
            public void handle(CellEditEvent<Method, String> t) {
                DataManager dataManager = (DataManager) app.getDataComponent();
                String currentTableValue = t.getOldValue();

                boolean previousLine = false;
                for (String x : dataManager.getselectedClass().getClassPane().getUses()) {
                    if (x.equals(currentTableValue)) {
                        previousLine = true;
                    }
                }
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setparg10TypeProperty(t.getNewValue());
                dataManager.getselectedClass().setPaneText();
                if (t.getNewValue().equals("") || t.getNewValue().equals(null)) {
                    methodArg10.setVisible(false);
                }
                boolean inDesign = false;
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                for (JcdClass x : dataManager.getClasses()) {
                    if (t.getNewValue().equals(x.getClassName())) {
                        inDesign = true;
                        if (previousLine) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                            selectedClassPane.getUses().remove(currentTableValue);
                        }
                        Line useLine = new Line();
                        useLine.getStrokeDashArray().addAll(2d, 11d);
                        if (dataManager.getselectedClass() != null) {
                            JcdClass selectedClass = dataManager.getselectedClass();
                            JcdClassPane selectedPane = selectedClass.getClassPane();
                            useLine.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
                            useLine.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));

                            useLine.endXProperty().bind(x.getClassPane().layoutXProperty());
                            useLine.endYProperty().bind(x.getClassPane().layoutYProperty());

                            selectedPane.getUsesLines().add(useLine);
                            x.getClassPane().getUsesLines().add(useLine);

                            selectedPane.getUses().add(x.getClassName());

                            getDesignPane().getChildren().add(useLine);
                            break;
                        }

                    }

                }
                if (previousLine) {
                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(currentTableValue)));
                        selectedClassPane.getUses().remove(currentTableValue);
                    }
                }
                if (inDesign == false) {
                    if (Character.isUpperCase(t.getNewValue().charAt(0))) {
                        TopToolbarController.processAPIUses(t.getNewValue());
                    }
                }
            }
        }
        );

        methodEditingPane.getChildren().add(methodTablePane);
        methodTablePane.getChildren().add(methodTable);

        methodArg3.setVisible(false);
        methodArg4.setVisible(false);
        methodArg5.setVisible(false);
        methodArg6.setVisible(false);
        methodArg7.setVisible(false);
        methodArg8.setVisible(false);
        methodArg9.setVisible(false);
        methodArg10.setVisible(false);

        methodTable.getColumns().addAll(methodName, methodReturn, methodScope, methodAbstract, methodAccess, methodArg1, methodArg2, methodArg3, methodArg4, methodArg5, methodArg6, methodArg7, methodArg8, methodArg9, methodArg10);

        // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
        DataManager data = (DataManager) app.getDataComponent();
        data.setClassPanes(scrollPaneContent.getChildren());
        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(scrollPane);
        ((BorderPane) workspace).setRight(controlPane);

    }

    public Pane getDesignPane() {
        return scrollPaneContent;
    }

    public TableView getVarTable() {
        return variableTable;
    }

    public TableView getMethTable() {
        return methodTable;
    }

    public TableColumn<Method, String> getMethodArg1col() {
        return methodArg1;
    }

    public TableColumn<Method, String> getMethodArg2col() {
        return methodArg2;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    public TextField getClassNameTF() {
        return classNameTA;
    }

    public TextField getPackageNameTF() {
        return packageNameTA;
    }

    public TextField getExternalParentTF() {
        return externalParentTA;
    }

    public TextField getExternalInterfaceTF() {
        return externalInterfaceTA;
    }

    public MenuButton getParentCB() {
        return parentChoice;
    }

    public MenuButton getInterfaceCB() {
        return interfaceChoice;
    }

    public boolean isSelectingState() {
        return selectingState;
    }

    public void setSelectingStateTrue() {
        selectingState = true;
    }

    public void setSelectingStateFalse() {
        selectingState = false;
    }

    public boolean isResizingState() {
        return resizingState;
    }

    public void setResizingStateTrue() {
        resizingState = true;
    }

    public void setResizingStateFalse() {
        resizingState = false;
    }

    public void setDebugText(String text) {
    }

    private void setupHandlers() throws IOException {
        // MAKE THE EDIT CONTROLLER

        DataManager dataManager = (DataManager) app.getDataComponent();
        TopToolbarController = new TopToolbarController(app);
        ArrayList<JcdClass> classes = dataManager.getClasses();

        classNameTA.setOnKeyReleased(e -> {
            if (dataManager.getselectedClass() != null) {
                boolean samePackage = false;

                if (classes.size() > 1 && dataManager.getselectedClass().getPackageName() != null) {
                    for (int i = 0; i < classes.size(); i++) {
                        JcdClass currentClass = classes.get(i);
                        if (currentClass.equals(dataManager.getselectedClass())) {
                            continue;
                        }
                        if (currentClass.getPackageName() != null) {
                            if (currentClass.getPackageName().equals(dataManager.getselectedClass().getPackageName())) {
                                samePackage = true;
                            }
                        }
                    }

                    if (samePackage) {
                        for (int i = 0; i < classes.size(); i++) {
                            JcdClass currentClass = classes.get(i);
                            if (currentClass.equals(dataManager.getselectedClass())) {
                                continue;
                            }
                            String currentClassName = currentClass.getClassName();
                            if (currentClassName.equals(classNameTA.getText())) {
                                Alert alert = new Alert(Alert.AlertType.WARNING);

                                alert.setTitle("Error");
                                alert.setHeaderText("This class/class already exists in the current design");
                                alert.setContentText("Change selected class name!");
                                classNameTA.clear();
                                alert.showAndWait();
                            }
                        }
                    }
                }

                TopToolbarController.processNameEditing();
            }

        });
        packageNameTA.setOnKeyReleased(e -> {
            if (dataManager.getselectedClass() != null) {
                boolean sameClass = false;

                if (classes.size() > 1) {
                    for (int i = 0; i < classes.size(); i++) {
                        JcdClass currentClass = classes.get(i);
                        if (currentClass.equals(dataManager.getselectedClass())) {
                            continue;
                        }
                        if (currentClass.getClassName().equals(dataManager.getselectedClass().getClassName())) {
                            sameClass = true;
                        }
                    }

                    if (sameClass) {
                        for (int i = 0; i < classes.size(); i++) {
                            JcdClass currentClass = classes.get(i);
                            if (currentClass.equals(dataManager.getselectedClass())) {
                                continue;
                            }
                            String currentPackageName = currentClass.getPackageName();
                            if (currentPackageName != null) {
                                if (currentPackageName.equals(packageNameTA.getText())) {
                                    Alert alert = new Alert(Alert.AlertType.WARNING);

                                    alert.setTitle("Error");
                                    alert.setHeaderText("This package/class combo already exists in the current design");
                                    alert.setContentText("Change selected package name!");
                                    packageNameTA.clear();
                                    alert.showAndWait();
                                }
                            }
                        }
                    }
                }
                TopToolbarController.processPackageEditing();
            }
        });

        externalParentTA.setOnKeyReleased(e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                TopToolbarController.processExternalParent();
            }
        });

        externalInterfaceTA.setOnKeyReleased(e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                TopToolbarController.processExternalInterface();
            }
        });

        variableAdd.setOnAction(e -> {
            if (dataManager.getselectedClass() != null) {
            dataManager.getselectedClass().getVariables().add(new Variable("myvar", "int", "public", false));
            variableTable.setItems(FXCollections.observableList(dataManager.getselectedClass().getVariables()));
            dataManager.getselectedClass().setPaneText();
            }

        });

        variableDelete.setOnAction(e -> {
            if (dataManager.getselectedClass() != null) {
            TablePosition firstCell = (TablePosition) variableTable.getSelectionModel().getSelectedCells().get(0);
            //int col = firstCell.getColumn();
            int row = firstCell.getRow();

            String varType = variableTable.getItems().get(row).getType();

            int varTypeCount = 0;

            for (int i = 0; i < variableTable.getItems().size(); i++) {
                if (variableTable.getItems().get(i).getType().equals(varType)) {
                    varTypeCount++;
                }
            }

            JcdClass selectedClass = dataManager.getselectedClass();
            JcdClassPane selectedClassPane = selectedClass.getClassPane();

            boolean inDesign = false;
            for (JcdClass currentClass : dataManager.getClasses()) {
                if (currentClass.getClassName().equals(varType)) {
                    inDesign = true;
                    break;
                }
            }
            if (varTypeCount == 1) {
                if (!Character.isLowerCase(varType.charAt(0))) {

                    if (inDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(varType)));
                        dataManager.getClassPanes().remove(selectedClassPane.getAggregatePanes().get(selectedClassPane.getAggregates().indexOf(varType)));
                        selectedClassPane.getAggregateLines().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(varType)));
                        selectedClassPane.getAggregatePanes().remove(selectedClassPane.getAggregatePanes().get(selectedClassPane.getAggregates().indexOf(varType)));
                        selectedClassPane.getAggregates().remove(varType);
                    } else {
                        dataManager.getClassPanes().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(varType)));
                        selectedClassPane.getAggregateLines().remove(selectedClassPane.getAggregateLines().get(selectedClassPane.getAggregates().indexOf(varType)));
                        selectedClassPane.getAggregates().remove(varType);
                    }

                }
            }

            variableTable.getItems().remove(row);
            dataManager.getselectedClass().setPaneText();
            }

        });

        methodAdd.setOnAction(e -> {
            if (dataManager.getselectedClass() != null) {

            Method currentMethod = new Method();

            currentMethod.setName("myMeth");
            currentMethod.initializePName();
            currentMethod.setReturn("void");
            currentMethod.initalizePReturnType();
            currentMethod.setAccess("public");
            currentMethod.initializePAccess();
            currentMethod.setAbstractMethod(false);
            currentMethod.initalizePAbstractMethod();
            currentMethod.setStaticMethod(false);
            currentMethod.initalizePStaticMethod();

            Argument arg1 = new Argument("myArg", "int");
            currentMethod.addArg(arg1);
            Argument arg2 = new Argument("", "");
            currentMethod.addArg(arg2);
            Argument arg3 = new Argument("", "");
            currentMethod.addArg(arg3);
            Argument arg4 = new Argument("", "");
            currentMethod.addArg(arg4);
            Argument arg5 = new Argument("", "");
            currentMethod.addArg(arg5);
            Argument arg6 = new Argument("", "");
            currentMethod.addArg(arg6);
            Argument arg7 = new Argument("", "");
            currentMethod.addArg(arg7);
            Argument arg8 = new Argument("", "");
            currentMethod.addArg(arg8);
            Argument arg9 = new Argument("", "");
            currentMethod.addArg(arg9);
            Argument arg10 = new Argument("", "");
            currentMethod.addArg(arg10);
            currentMethod.initalizePArgs();

            dataManager.getselectedClass().getMethods().add(currentMethod);
            methodTable.setItems(FXCollections.observableList(dataManager.getselectedClass().getMethods()));
            dataManager.getselectedClass().setPaneText();
            }
        });

        methodDelete.setOnAction(e -> {
            if (dataManager.getselectedClass() != null) {
            TablePosition firstCell = (TablePosition) methodTable.getSelectionModel().getSelectedCells().get(0);
            int col = firstCell.getColumn();
            int row = firstCell.getRow();

            String mReturn = methodTable.getItems().get(row).getReturnType();
            String arg1Type = methodTable.getItems().get(row).parg1TypeProperty().get();
            String arg2Type = methodTable.getItems().get(row).parg2TypeProperty().get();
            String arg3Type = methodTable.getItems().get(row).parg3TypeProperty().get();
            String arg4Type = methodTable.getItems().get(row).parg4TypeProperty().get();
            String arg5Type = methodTable.getItems().get(row).parg5TypeProperty().get();
            String arg6Type = methodTable.getItems().get(row).parg6TypeProperty().get();
            String arg7Type = methodTable.getItems().get(row).parg7TypeProperty().get();
            String arg8Type = methodTable.getItems().get(row).parg8TypeProperty().get();
            String arg9Type = methodTable.getItems().get(row).parg9TypeProperty().get();
            String arg10Type = methodTable.getItems().get(row).parg10TypeProperty().get();

            int mReturnCount = 0;
            int arg1Count = 0;
            int arg2Count = 0;
            int arg3Count = 0;
            int arg4Count = 0;
            int arg5Count = 0;
            int arg6Count = 0;
            int arg7Count = 0;
            int arg8Count = 0;
            int arg9Count = 0;
            int arg10Count = 0;

            for (int i = 0; i < methodTable.getItems().size(); i++) {
                if (mReturn.equals(methodTable.getItems().get(i).getReturnType())) {
                    mReturnCount++;
                }
                if (arg1Type.equals(methodTable.getItems().get(i).parg1TypeProperty().get())) {
                    if (!arg1Type.equals("") && !arg1Type.equals(null)) {
                        arg1Count++;
                    }
                }
                if (arg2Type.equals(methodTable.getItems().get(i).parg2TypeProperty().get())) {
                    if (!arg2Type.equals("") && !arg2Type.equals(null)) {
                        arg2Count++;
                    }
                }
                if (arg3Type.equals(methodTable.getItems().get(i).parg3TypeProperty().get())) {
                    if (!arg3Type.equals("") && !arg3Type.equals(null)) {
                        arg3Count++;
                    }
                }
                if (arg4Type.equals(methodTable.getItems().get(i).parg4TypeProperty().get())) {
                    if (!arg4Type.equals("") && !arg4Type.equals(null)) {
                        arg4Count++;
                    }
                }
                if (arg5Type.equals(methodTable.getItems().get(i).parg5TypeProperty().get())) {
                    if (!arg5Type.equals("") && !arg5Type.equals(null)) {
                        arg5Count++;
                    }
                }
                if (arg6Type.equals(methodTable.getItems().get(i).parg6TypeProperty().get())) {
                    if (!arg6Type.equals("") && !arg6Type.equals(null)) {
                        arg6Count++;
                    }
                }
                if (arg7Type.equals(methodTable.getItems().get(i).parg7TypeProperty().get())) {
                    if (!arg7Type.equals("") && !arg7Type.equals(null)) {
                        arg7Count++;
                    }
                }
                if (arg8Type.equals(methodTable.getItems().get(i).parg8TypeProperty().get())) {
                    if (!arg8Type.equals("") && !arg8Type.equals(null)) {
                        arg8Count++;
                    }
                }
                if (arg9Type.equals(methodTable.getItems().get(i).parg9TypeProperty().get())) {
                    if (!arg9Type.equals("") && !arg9Type.equals(null)) {
                        arg9Count++;
                    }
                }
                if (arg10Type.equals(methodTable.getItems().get(i).parg10TypeProperty().get())) {
                    if (!arg10Type.equals("") && !arg10Type.equals(null)) {
                        arg10Count++;
                    }
                }
            }

            boolean returnInDesign = false;
            boolean arg1InDesign = false;
            boolean arg2InDesign = false;
            boolean arg3InDesign = false;
            boolean arg4InDesign = false;
            boolean arg5InDesign = false;
            boolean arg6InDesign = false;
            boolean arg7InDesign = false;
            boolean arg8InDesign = false;
            boolean arg9InDesign = false;
            boolean arg10InDesign = false;

            JcdClass selectedClass = dataManager.getselectedClass();
            JcdClassPane selectedClassPane = selectedClass.getClassPane();

            for (JcdClass currentClass : dataManager.getClasses()) {
                if (currentClass.getClassName().equals(mReturn)) {
                    returnInDesign = true;
                }
                if (arg1Type.equals(currentClass.getClassName())) {
                    arg1InDesign = true;
                }
                if (arg2Type.equals(currentClass.getClassName())) {
                    arg2InDesign = true;
                }
                if (arg3Type.equals(currentClass.getClassName())) {
                    arg3InDesign = true;
                }
                if (arg4Type.equals(currentClass.getClassName())) {
                    arg4InDesign = true;
                }
                if (arg5Type.equals(currentClass.getClassName())) {
                    arg5InDesign = true;
                }
                if (arg6Type.equals(currentClass.getClassName())) {
                    arg6InDesign = true;
                }
                if (arg7Type.equals(currentClass.getClassName())) {
                    arg7InDesign = true;
                }
                if (arg8Type.equals(currentClass.getClassName())) {
                    arg8InDesign = true;
                }
                if (arg9Type.equals(currentClass.getClassName())) {
                    arg9InDesign = true;
                }
                if (arg10Type.equals(currentClass.getClassName())) {
                    arg10InDesign = true;
                }
            }

            if (mReturnCount == 1) {
                if (!Character.isLowerCase(mReturn.charAt(0))) {
                    if (returnInDesign == false) {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(mReturn)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(mReturn)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(mReturn)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(mReturn)));
                        selectedClassPane.getUses().remove(mReturn);

                    } else {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(mReturn)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(mReturn)));
                        selectedClassPane.getUses().remove(mReturn);

                    }
                }
            }
            if (arg1Count == 1) {
                if (!Character.isLowerCase(arg1Type.charAt(0))) {
                    if (arg1InDesign == false) {

                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        selectedClassPane.getUses().remove(arg1Type);

                    } else {
                        dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg1Type)));
                        selectedClassPane.getUses().remove(arg1Type);

                    }
                }

            }
            try {
                if (arg2Count == 1) {
                    if (!Character.isLowerCase(arg2Type.charAt(0))) {
                        if (arg2InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            selectedClassPane.getUses().remove(arg2Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg2Type)));
                            selectedClassPane.getUses().remove(arg2Type);

                        }

                    }
                }
                if (arg3Count == 1) {
                    if (!Character.isLowerCase(arg3Type.charAt(0))) {
                        if (arg3InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            selectedClassPane.getUses().remove(arg3Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg3Type)));
                            selectedClassPane.getUses().remove(arg3Type);

                        }

                    }
                }
                if (arg4Count == 1) {
                    if (!Character.isLowerCase(arg4Type.charAt(0))) {
                        if (arg4InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            selectedClassPane.getUses().remove(arg4Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg4Type)));
                            selectedClassPane.getUses().remove(arg4Type);

                        }

                    }
                }
                if (arg5Count == 1) {
                    if (!Character.isLowerCase(arg5Type.charAt(0))) {
                        if (arg5InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            selectedClassPane.getUses().remove(arg5Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg5Type)));
                            selectedClassPane.getUses().remove(arg5Type);

                        }

                    }
                }
                if (arg6Count == 1) {
                    if (!Character.isLowerCase(arg6Type.charAt(0))) {
                        if (arg6InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            selectedClassPane.getUses().remove(arg6Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg6Type)));
                            selectedClassPane.getUses().remove(arg6Type);

                        }

                    }
                }
                if (arg7Count == 1) {
                    if (!Character.isLowerCase(arg7Type.charAt(0))) {
                        if (arg7InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            selectedClassPane.getUses().remove(arg7Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg7Type)));
                            selectedClassPane.getUses().remove(arg7Type);

                        }

                    }
                }
                if (arg8Count == 1) {
                    if (!Character.isLowerCase(arg8Type.charAt(0))) {
                        if (arg8InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            selectedClassPane.getUses().remove(arg8Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg8Type)));
                            selectedClassPane.getUses().remove(arg8Type);

                        }

                    }
                }
                if (arg9Count == 1) {
                    if (!Character.isLowerCase(arg9Type.charAt(0))) {
                        if (arg9InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            selectedClassPane.getUses().remove(arg9Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg9Type)));
                            selectedClassPane.getUses().remove(arg9Type);

                        }

                    }
                }
                if (arg10Count == 1) {
                    if (!Character.isLowerCase(arg10Type.charAt(0))) {
                        if (arg10InDesign == false) {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            selectedClassPane.getUsesPanes().remove(selectedClassPane.getUsesPanes().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            selectedClassPane.getUses().remove(arg10Type);

                        } else {
                            dataManager.getClassPanes().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            selectedClassPane.getUsesLines().remove(selectedClassPane.getUsesLines().get(selectedClassPane.getUses().indexOf(arg10Type)));
                            selectedClassPane.getUses().remove(arg10Type);

                        }

                    }
                }
            } catch (IndexOutOfBoundsException | NullPointerException eyy) {
                methodTable.getItems().remove(row);

                dataManager.getselectedClass().setPaneText();

                if (methodArg3.getCellData(row) != null) {
                    if (methodArg3.getCellData(row).equals("") || methodArg3.getCellData(row) == (null)) {
                        methodArg3.setVisible(false);
                    }
                }
                if (methodArg4.getCellData(row) != null) {
                    if (methodArg4.getCellData(row).equals("") || methodArg4.getCellData(row) == (null)) {
                        methodArg4.setVisible(false);
                    }
                }
                if (methodArg5.getCellData(row) != null) {
                    if (methodArg5.getCellData(row).equals("") || methodArg5.getCellData(row) == (null)) {
                        methodArg5.setVisible(false);
                    }
                }
                if (methodArg6.getCellData(row) != null) {
                    if (methodArg6.getCellData(row).equals("") || methodArg6.getCellData(row) == (null)) {
                        methodArg6.setVisible(false);
                    }
                }
                if (methodArg7.getCellData(row) != null) {
                    if (methodArg7.getCellData(row).equals("") || methodArg7.getCellData(row) == (null)) {
                        methodArg7.setVisible(false);
                    }
                }
                if (methodArg8.getCellData(row) != null) {
                    if (methodArg8.getCellData(row).equals("") || methodArg8.getCellData(row) == (null)) {
                        methodArg8.setVisible(false);
                    }
                }
                if (methodArg9.getCellData(row) != null) {
                    if (methodArg9.getCellData(row).equals("") || methodArg9.getCellData(row) == (null)) {
                        methodArg9.setVisible(false);
                    }
                }
                if (methodArg10.getCellData(row) != null) {
                    if (methodArg10.getCellData(row).equals("") || methodArg3.getCellData(row) == (null)) {
                        methodArg10.setVisible(false);
                    }
                }
            }
            methodTable.getItems().remove(row);

            dataManager.getselectedClass().setPaneText();

            if (methodArg3.getCellData(row) != null) {
                if (methodArg3.getCellData(row).equals("") || methodArg3.getCellData(row) == (null)) {
                    methodArg3.setVisible(false);
                }
            }
            if (methodArg4.getCellData(row) != null) {
                if (methodArg4.getCellData(row).equals("") || methodArg4.getCellData(row) == (null)) {
                    methodArg4.setVisible(false);
                }
            }
            if (methodArg5.getCellData(row) != null) {
                if (methodArg5.getCellData(row).equals("") || methodArg5.getCellData(row) == (null)) {
                    methodArg5.setVisible(false);
                }
            }
            if (methodArg6.getCellData(row) != null) {
                if (methodArg6.getCellData(row).equals("") || methodArg6.getCellData(row) == (null)) {
                    methodArg6.setVisible(false);
                }
            }
            if (methodArg7.getCellData(row) != null) {
                if (methodArg7.getCellData(row).equals("") || methodArg7.getCellData(row) == (null)) {
                    methodArg7.setVisible(false);
                }
            }
            if (methodArg8.getCellData(row) != null) {
                if (methodArg8.getCellData(row).equals("") || methodArg8.getCellData(row) == (null)) {
                    methodArg8.setVisible(false);
                }
            }
            if (methodArg9.getCellData(row) != null) {
                if (methodArg9.getCellData(row).equals("") || methodArg9.getCellData(row) == (null)) {
                    methodArg9.setVisible(false);
                }
            }
            if (methodArg10.getCellData(row) != null) {
                if (methodArg10.getCellData(row).equals("") || methodArg3.getCellData(row) == (null)) {
                    methodArg10.setVisible(false);
                }
            }
            }
        });

        // NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        /*
        0 - New, 1- Load, 2- Save, 3- SaveAs, 4- Photo, 5-Code, 6-Exit
        7- Select, 8-Resize, 9-Add Class, 10-Add Interface, 11-Remove, 12-Undo, 13-Redo
        14- Zoom In, 15, Zoom-Out
         */
        buttons = gui.getButtons();
        buttons.get(4).setOnAction(e -> {
            TopToolbarController.processSnapshot();
        });
        buttons.get(5).setOnAction(e -> {
            try {
                TopToolbarController.processExportCode();
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        buttons.get(7).setOnAction(e -> {
            TopToolbarController.processSelectSelectionTool();
            selectingState = true;
        });

        buttons.get(8).setOnAction(e -> {
            resizingState = true;
        });
        buttons.get(9).setOnAction(e -> {
            TopToolbarController.processAddClass();
        });
        buttons.get(10).setOnAction(e -> {
            TopToolbarController.processAddInterface();
        });

        buttons.get(11).setOnAction(e -> {
            TopToolbarController.processRemove();
        });

        CheckBox gridRender = gui.getGridRenderCB();

        gridRender.setOnAction(e -> {
            if (this.gridLinesRendered == false) {
                gridLines = new GridPane();
                gridLines.setAlignment(Pos.TOP_LEFT);
                int rows = 200;
                int columns = 200;
                for (int i = 0; i < columns; i++) {
                    ColumnConstraints column = new ColumnConstraints(50);
                    column.setHalignment(HPos.LEFT);
                    gridLines.getColumnConstraints().add(column);
                }

                for (int i = 0; i < rows; i++) {
                    RowConstraints row = new RowConstraints(50);
                    row.setValignment(VPos.TOP);
                    gridLines.getRowConstraints().add(row);

                }
                gridLines.setGridLinesVisible(true);
                scrollPane.setContent(gridLines);
                gridLines.getChildren().add(scrollPaneContent);

                gridLines.setSnapToPixel(true);
                gridLinesRendered = true;
            } else if (gridLinesRendered == true) {
                scrollPane.setContent(scrollPaneContent);
                gridLinesRendered = false;
            }
        });

        buttons.get(14).setOnAction(e -> {
            scrollPaneContent.setScaleX(scrollPaneContent.getScaleX() * 2);
            scrollPaneContent.setScaleY(scrollPaneContent.getScaleY() * 2);

        });
        buttons.get(15).setOnAction(e -> {
            scrollPaneContent.setScaleX(scrollPaneContent.getScaleX() / 2);
            scrollPaneContent.setScaleY(scrollPaneContent.getScaleY() / 2);
        });

        scrollPane.setOnMousePressed(e -> {
            for (int i = 0; i < classes.size(); i++) {
                if (classes.get(i).getClassName().equals("")) {
                    classes.get(i).setClassName("Dummy");
                    classes.get(i).getClassPane().getClassTA().setText("Dummy");
                }
            }

            int ex = (int) e.getX();
            int ey = (int) e.getY();
            if (dataManager.getselectedClass() != null) {
                JcdClassPane selectedClassPane = dataManager.getselectedClass().getClassPane();
                int x = (int) selectedClassPane.getLayoutX();
                int y = (int) selectedClassPane.getLayoutY();

                if (ex != x) {//ey > y) {
                    dataManager.unhighlightClass(selectedClassPane);
                    classNameTA.setPromptText("");
                    packageNameTA.setPromptText("");
                    classNameTA.clear();
                    packageNameTA.clear();
                    externalParentTA.clear();
                    externalInterfaceTA.clear();
                    parentChoice.setText("");
                    interfaceChoice.setText("");
                    variableTable.setItems(null);
                    methodTable.setItems(null);
                    dataManager.setSelectedClass(null);
                }
            }

        });

        scrollPane.setOnMouseMoved(e -> {
            int ex = (int) e.getX();
            int ey = (int) e.getY();
            if (dataManager.getselectedClass() != null) {
                JcdClassPane pane = dataManager.getselectedClass().getClassPane();
                if (((ex >= pane.getLayoutX()) && (ex <= pane.getLayoutX() + 5)) && ((ey >= pane.getLayoutY()) && (ey <= pane.getLayoutY() + 5))) {
                    //if(ex == pane.getLayoutX() && ey == pane.getLayoutY()) {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.NW_RESIZE);
                    setResizingStateTrue();
                } //Make easier to resize
                else if (ex == pane.getLayoutX() && ey == (pane.getLayoutY() + pane.getPrefHeight())) {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.SW_RESIZE);
                    setResizingStateTrue();
                } // Make easier to resize
                else if (ex == (pane.getLayoutX() + pane.getPrefWidth()) && ey == (pane.getLayoutY() + pane.getPackagePane().getPrefHeight())) {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.NE_RESIZE);
                    setResizingStateTrue();
                } // Make easier to resize
                else if (ex == (pane.getLayoutX() + pane.getPrefWidth()) && ey == (pane.getLayoutY() + pane.getPrefHeight())) {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.SE_RESIZE);
                    setResizingStateTrue();
                } else {
                    Scene scene = app.getGUI().getPrimaryScene();
                    scene.setCursor(Cursor.DEFAULT);
                    setResizingStateFalse();

                }
            } else {
                Scene scene = app.getGUI().getPrimaryScene();
                scene.setCursor(Cursor.DEFAULT);
                setResizingStateFalse();

            }
        });

        // MAKE THE CANVAS CONTROLLER	
    }

    public void setImage(ButtonBase button, String fileName) {
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE

        scrollPane.getStyleClass().add("scroll_pane");
        controlPane.getStyleClass().add("right_control_pane");
        varMethodEditingPane.getStyleClass().add("right_control_pane");
        variableEditingPane.getStyleClass().add("right_control_pane");
        methodEditingPane.getStyleClass().add("right_control_pane");
        topEditingPane.getStyleClass().add("grid_pane");
        classNameLabel.getStyleClass().add("labels");
        packageLabel.getStyleClass().add("labels");
        parentLabel.getStyleClass().add("labels");
        externalParent.getStyleClass().add("labels");
        externalInterface.getStyleClass().add("labels");
        interfaceLabel.getStyleClass().add("labels");
        variablesLabel.getStyleClass().add("labels");
        methodsLabel.getStyleClass().add("labels");
        variableAdd.getStyleClass().add("plus");
        variableDelete.getStyleClass().add("minus");
        methodAdd.getStyleClass().add("plus");
        methodDelete.getStyleClass().add("minus");
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        /*
        0 - New, 1- Load, 2- Save, 3- SaveAs, 4- Photo, 5-Code, 6-Exit
        7- Select, 8-Resize, 9-Add Class, 10-Add Interface, 11-Remove, 12-Undo, 13-Redo
        14- Zoom In, 15, Zoom-Out
         */

        ArrayList<Button> buttons = app.getGUI().getButtons();

    }

    public void loadSelectedClassSettings(JcdClass selectedClass) {
        classNameTA.setPromptText(selectedClass.getClassName());
        packageNameTA.setPromptText(selectedClass.getPackageName());
    }

    @Override
    public void resetWorkspace() {
        if (classNameTA != null);
        classNameTA.setPromptText("");
        if (packageNameTA != null) {
            packageNameTA.setPromptText("");
        }
        if (externalParentTA != null) {
            externalParentTA.setPromptText("");
        }
        if (externalInterface != null) {
            externalInterfaceTA.setPromptText("");
        }

        parentChoice.getItems().clear();
        interfaceChoice.getItems().clear();
        variableTable.getItems().clear();
        methodTable.getItems().clear();
    }
        
}
