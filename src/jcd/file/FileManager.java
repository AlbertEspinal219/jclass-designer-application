package jcd.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.Argument;
import jcd.data.DataManager;
import jcd.data.JcdClass;
import jcd.data.JcdClassPane;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    // FOR JSON LOADING

    AppTemplate app;

    static final String JSON_CLASSES = "classes";
    static final String JSON_CLASS_NAME = "class_name";
    static final String JSON_PACKAGE_NAME = "package_name";
    static final String JSON_ABSTRACT = "abstract";
    static final String JSON_INTERFACE = "interface";
    static final String JSON_PARENT = "parent_class";
    static final String JSON_CHILD = "child_class";
    static final String JSON_VARIABLES = "variables";
    static final String JSON_METHODS = "methods";
    static final String JSON_VAR_NAME = "variable_name";
    static final String JSON_VAR_TYPE = "variable_tyle";
    static final String JSON_VAR_STATIC = "variable_static";
    static final String JSON_VAR_ACCESS = "variable_access";
    static final String JSON_METH_NAME = "method_name";
    static final String JSON_METH_RETURN = "method_return";
    static final String JSON_METH_STATIC = "method_static";
    static final String JSON_METH_ABSTRACT = "method_abstract";
    static final String JSON_METH_ACCESS = "method_access";
    static final String JSON_METH_ARGUMENTS = "method_arguments";
    static final String JSON_METH_ARG_TYPE = "argument_type";
    static final String JSON_METH_ARG_NAME = "argument_name";
    static final String JSON_PANES = "class_pane";
    static final String JSON_PANE_X = "pane_x";
    static final String JSON_PANE_Y = "pane_y";
    static final String JSON_PANE_H = "pane_height";
    static final String JSON_PANE_W = "pane_width";

    public FileManager(AppTemplate initApp) {
        app = initApp;
    }

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {

        DataManager dataManager = (DataManager) data;
        JsonArrayBuilder classData = Json.createArrayBuilder();

        for (JcdClass jcdClass : dataManager.getClasses()) {
            JcdClass currentClass = (JcdClass) jcdClass;
            String className = currentClass.getClassName();
            String packageName = currentClass.getPackageName();

            if (packageName == null) {
                packageName = "";
            }
            ArrayList<Variable> variables = currentClass.getVariables();
            ArrayList<Method> methods = currentClass.getMethods();

            String parent = currentClass.getParent();
            String child = currentClass.getChild();

            boolean isAbstract = currentClass.isIsAbstract();
            boolean isInterface = currentClass.isIsAbstract();

            double paneX = currentClass.getClassPane().getLayoutX();
            double paneY = currentClass.getClassPane().getLayoutY();
            double paneHeight = currentClass.getClassPane().getHeight();
            double paneWidth = currentClass.getClassPane().getWidth();

            // THEN THE VARIABLES
            JsonArrayBuilder arrayBuilderVars = Json.createArrayBuilder();
            fillArrayWithVariables(variables, arrayBuilderVars);
            JsonArray variablesArray = arrayBuilderVars.build();

            JsonArrayBuilder arrayBuilderMeths = Json.createArrayBuilder();
            fillArrayWithMethods(methods, arrayBuilderMeths);
            JsonArray methodsArray = arrayBuilderMeths.build();

            JsonObject paneData = makePaneJsonObject(paneX, paneY, paneHeight, paneWidth);

            JsonObject classJson = Json.createObjectBuilder()
                    .add(JSON_CLASS_NAME, className)
                    .add(JSON_PACKAGE_NAME, packageName)
                    .add(JSON_ABSTRACT, isAbstract)
                    .add(JSON_INTERFACE, isInterface)
                    .add(JSON_PARENT, parent)
                    .add(JSON_CHILD, child)
                    .add(JSON_VARIABLES, variablesArray)
                    .add(JSON_METHODS, methodsArray)
                    .add(JSON_PANES, paneData)
                    .build();
            classData.add(classJson);

        }

        JsonArray classesArray = classData.build();

        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_CLASSES, classesArray)
                .build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    private static JsonObject makeVariableJsonObject(Variable var) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_VAR_NAME, var.getName())
                .add(JSON_VAR_TYPE, var.getType())
                .add(JSON_VAR_STATIC, var.isStatic())
                .add(JSON_VAR_ACCESS, var.getAccessibility())
                //.add(JSON_VAR_ACCESS, var.getAccessibility())
                .build();
        return jso;
    }

    private static JsonObject makeArgumentJsonObject(Argument arg) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_METH_ARG_TYPE, arg.getArgType())
                .add(JSON_METH_ARG_NAME, arg.getArgName())
                .build();
        return jso;
    }

    private static JsonObject makeMethodJsonObject(Method meth) {

        ArrayList<Argument> args = meth.getArguments();
        JsonArrayBuilder arrayBuilderArgs = Json.createArrayBuilder();
        if (args.size() > 0) {
            fillArrayWithArgs(args, arrayBuilderArgs);
        }

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_METH_NAME, meth.getName())
                .add(JSON_METH_RETURN, meth.getReturnType())
                .add(JSON_METH_STATIC, meth.isStatic())
                .add(JSON_METH_ABSTRACT, meth.isAbstract())
                .add(JSON_METH_ACCESS, meth.getAccess())
                .add(JSON_METH_ARGUMENTS, arrayBuilderArgs)
                .build();

        return jso;
    }

    private static void fillArrayWithVariables(ArrayList<Variable> variables, JsonArrayBuilder arrayBuilder) {

        for (Variable i : variables) {
            JsonObject variableObject = makeVariableJsonObject(i);
            arrayBuilder.add(variableObject);
        }
    }

    private static void fillArrayWithMethods(ArrayList<Method> methods, JsonArrayBuilder arrayBuilder) {
        for (Method i : methods) {
            JsonObject variableObject = makeMethodJsonObject(i);
            arrayBuilder.add(variableObject);
        }
    }

    private static void fillArrayWithArgs(ArrayList<Argument> args, JsonArrayBuilder arrayBuilder) {
        for (Argument i : args) {
            if (i != null) {
                JsonObject argumentObject = makeArgumentJsonObject(i);
                arrayBuilder.add(argumentObject);
            }

        }
    }

    private static JsonObject makePaneJsonObject(double pX, double pY, double pH, double pW) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_PANE_X, pX)
                .add(JSON_PANE_Y, pY)
                .add(JSON_PANE_H, pH)
                .add(JSON_PANE_W, pW)
                .build();

        return jso;
    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        dataManager.reset();

        JsonObject json = loadJSONFile(filePath);

        JsonArray jsonClassArray = json.getJsonArray(JSON_CLASSES);

        for (int i = 0; i < jsonClassArray.size(); i++) {
            JsonObject jsonClass = jsonClassArray.getJsonObject(i);
            JcdClass currentClass = loadClass(jsonClass);
            JcdClassPane currentPane = loadPane(jsonClass);
            currentClass.setClassPane(currentPane);
            currentPane.setAssociatedClass(currentClass);
            setPaneText(currentClass);
            dataManager.getClasses().add(currentClass);

            currentPane.getClassTA().setText(currentClass.getClassName());
            currentPane.getPackageTA().setText(currentClass.getPackageName());
            makeSelectable(currentPane, data);
            makeDraggable(currentPane);
            
            Rectangle aggregation = new Rectangle();
            aggregation.setWidth(25);
            aggregation.setHeight(25);
            aggregation.getTransforms().add(new Rotate(45));

            aggregation.layoutXProperty().bind(currentPane.layoutXProperty().subtract(20));
            aggregation.layoutYProperty().bind(currentPane.layoutYProperty().add(currentPane.prefHeightProperty().divide(2).subtract(10)));


            currentPane.setAggregationConnecter(aggregation);
            dataManager.getClassPanes().add(aggregation);
            
            Polygon inheritence = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });

            inheritence.layoutXProperty().bind(currentPane.layoutXProperty().add(currentPane.prefWidthProperty().divide(2).subtract(22.5)));


            inheritence.layoutYProperty().bind(currentPane.layoutYProperty().add(currentPane.heightProperty().subtract(5)));

            currentPane.setInheritanceconnector(inheritence);
            dataManager.getClassPanes().add(inheritence);
            
            Polygon uses = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });
            uses.getTransforms().add(new Rotate(-90));

            uses.layoutXProperty().bind(currentPane.layoutXProperty().add(currentPane.prefWidthProperty()));


            uses.layoutYProperty().bind(currentPane.layoutYProperty().add(currentPane.prefHeightProperty().subtract(0)));


            currentPane.setUsesConnector(uses);
            dataManager.getClassPanes().add(uses);
            
            dataManager.addClassPane(currentPane);
        }
    }

    private static JcdClass loadClass(JsonObject jsonClass) {
        String className = jsonClass.getString(JSON_CLASS_NAME);
        String packageName = jsonClass.getString(JSON_PACKAGE_NAME);
        String parentName = jsonClass.getString(JSON_PARENT);
        String childName = jsonClass.getString(JSON_CHILD);
        boolean isAbstract = jsonClass.getBoolean(JSON_ABSTRACT);
        boolean isInterface = jsonClass.getBoolean(JSON_INTERFACE);
        ArrayList<Variable> variables = loadVariables(jsonClass);
        ArrayList<Method> methods = loadMethods(jsonClass);
        JcdClass newClass = new JcdClass();
        newClass.setParent(parentName);
        newClass.setChild(childName);
        newClass.setClassName(className);
        newClass.setPackageName(packageName);
        newClass.setVariables(variables);
        newClass.setMethods(methods);
        newClass.setIsAbstract(isAbstract);
        newClass.setIsInterface(isInterface);
        return newClass;
    }

    private void setPaneText(JcdClass jcdClass) {
        JcdClassPane classPane = jcdClass.getClassPane();
        ArrayList<Variable> vars = jcdClass.getVariables();
        ArrayList<Method> methds = jcdClass.getMethods();

        String varText = "";

        for (Variable v : vars) {
            varText += v.toString();
            varText += "\n";
        }
        String methodText = "";

        for (Method m : methds) {
            methodText += m.toString();
            methodText += "\n";
        }

        classPane.getVariablesTA().setText(varText);
        classPane.getMethodsTA().setText(methodText);

    }

    private static JcdClassPane loadPane(JsonObject jsonClass) {

        JsonObject paneData = jsonClass.getJsonObject(JSON_PANES);
        JcdClassPane pane = new JcdClassPane();
        double x = getDataAsDouble(paneData, JSON_PANE_X);
        double y = getDataAsDouble(paneData, JSON_PANE_Y);
        double height = 150;
        double width = 400;

        pane.setLocationAndSize(x, y, width, height);

        return pane;
    }

    private static double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    private static ArrayList<Variable> loadVariables(JsonObject jsonClass) {
        ArrayList<Variable> variables = new ArrayList();
        JsonArray jsonVariablesArray = jsonClass.getJsonArray(JSON_VARIABLES);

        for (int i = 0; i < jsonVariablesArray.size(); i++) {
            JsonObject jsonVariable = jsonVariablesArray.getJsonObject(i);
            String varName = jsonVariable.getString(JSON_VAR_NAME);
            String varType = jsonVariable.getString(JSON_VAR_TYPE);
            boolean varStatic = jsonVariable.getBoolean(JSON_VAR_STATIC);
            String varAccess = jsonVariable.getString(JSON_VAR_ACCESS);

            Variable currentVar = new Variable(varName, varType, varAccess, varStatic);
            variables.add(currentVar);

        }
        return variables;
    }

    private static ArrayList<Method> loadMethods(JsonObject jsonClass) {
        ArrayList<Method> methods = new ArrayList();
        JsonArray jsonMethodsArray = jsonClass.getJsonArray(JSON_METHODS);

        for (int i = 0; i < jsonMethodsArray.size(); i++) {
            JsonObject jsonMethod = jsonMethodsArray.getJsonObject(i);
            String methName = jsonMethod.getString(JSON_METH_NAME);
            String methReturn = jsonMethod.getString(JSON_METH_RETURN);
            boolean methStatic = jsonMethod.getBoolean(JSON_METH_STATIC);
            boolean methAbstract = jsonMethod.getBoolean(JSON_METH_ABSTRACT);
            String methAccess = jsonMethod.getString(JSON_METH_ACCESS);
            JsonArray argsArray = jsonMethod.getJsonArray(JSON_METH_ARGUMENTS);

            Method currentMethod = new Method();
            for (int j = 0; j < argsArray.size(); j++) {
                JsonObject jsonArg = argsArray.getJsonObject(j);
                String argType = jsonArg.getString(JSON_METH_ARG_TYPE);
                String argName = jsonArg.getString(JSON_METH_ARG_NAME);

                Argument newArg = new Argument(argName, argType);
                currentMethod.addArg(newArg);
            }

            for (int x = currentMethod.getArguments().size(); x < 10; x++) {
                Argument newArg = new Argument("", "");
                currentMethod.addArg(newArg);

            }

            currentMethod.initalizePArgs();

            currentMethod.setName(methName);
            currentMethod.initializePName();
            currentMethod.setReturn(methReturn);
            currentMethod.initalizePReturnType();
            currentMethod.setAccess(methAccess);
            currentMethod.initializePAccess();
            currentMethod.setAbstractMethod(methAbstract);
            currentMethod.initalizePAbstractMethod();
            currentMethod.setStaticMethod(methStatic);
            currentMethod.initalizePStaticMethod();

            methods.add(currentMethod);

        }
        return methods;
    }

    private static JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    public void makeDraggable(JcdClassPane classPane) {

        classPane.setOnMousePressed(e -> {

            Workspace workspace = (Workspace) (app.getWorkspaceComponent());
            boolean dragState = workspace.isSelectingState();
            if (dragState) {

                classPane.setMouseX(e.getSceneX());
                classPane.setMouseY(e.getSceneY());

                classPane.setX(classPane.getLayoutX());
                classPane.setY(classPane.getLayoutY());
            }
        });

        classPane.setOnMouseDragged(e -> {
            Workspace workspace = (Workspace) (app.getWorkspaceComponent());
            boolean dragState = workspace.isSelectingState();
            boolean resizeState = workspace.isResizingState();
            if (dragState) {

                double xDiff = e.getSceneX() - classPane.getMouseX();
                double yDiff = e.getSceneY() - classPane.getMouseY();

                if (resizeState) {
                    //classPane.getPackagePane().setPrefWidth( (classPane.getPrefWidth() + xDiff) / 5);
                    classPane.setPrefHeight(classPane.getPrefHeight() + yDiff);
                    classPane.setPrefWidth(classPane.getPrefWidth() + xDiff);
                }

                classPane.setX(classPane.getX() + xDiff);
                classPane.setY(classPane.getY() + yDiff);

                double X = classPane.getX();
                double Y = classPane.getY();

                if (resizeState == false) {
                    classPane.setLayoutX(X);
                    classPane.setLayoutY(Y);
                }

                classPane.setMouseX(e.getSceneX());
                classPane.setMouseY(e.getSceneY());

                e.consume();
            }
        });
    }

    public void makeSelectable(JcdClassPane classPane, AppDataComponent data) {
        DataManager dataManager = (DataManager) data;
        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        JcdClass newClass = classPane.getAssociatedClass();
        classPane.setOnMouseClicked(e -> {
            if (workspace.isSelectingState()) {
                int ex = (int) e.getX();
                int ey = (int) e.getY();

                if (app.getGUI().getGridRenderCB().isSelected()) {
                    if (app.getGUI().getSnapCB().isSelected()) {
                        classPane.setLayoutX(((int) classPane.getLayoutX() + 25) / 50 * 50);
                        classPane.setLayoutY(((int) classPane.getLayoutY() + 25) / 50 * 50);
                    }
                }
                if (!(((ex > classPane.getPackagePane().getPrefWidth()) && ex < classPane.getPrefWidth()) && (ey < classPane.getPackagePane().getPrefHeight()))) {
                    if (dataManager.getselectedClass() != null) {
                        dataManager.unhighlightClass(dataManager.getselectedClass().getClassPane());
                    }
                    dataManager.setSelectedClass(newClass);
                    dataManager.highlightClass(classPane);
                    if (newClass != null) {
                        workspace.getVarTable().setItems(FXCollections.observableList(classPane.getAssociatedClass().getVariables()));
                        workspace.getMethTable().setItems(FXCollections.observableList(classPane.getAssociatedClass().getMethods()));
                        workspace.getClassNameTF().setPromptText(newClass.getClassName());
                        workspace.getPackageNameTF().setPromptText(newClass.getPackageName());
                        workspace.getParentCB().setText(classPane.getAssociatedClass().getParent());
                        workspace.getExternalParentTF().setText(newClass.getExternalParent());

                        if (newClass.getInterfaces().size() > 0) {

                            for (MenuItem x : workspace.getInterfaceCB().getItems()) {
                                if (x instanceof CheckMenuItem) {
                                    CheckMenuItem cx = (CheckMenuItem) x;
                                    if (classPane.getAssociatedClass().getInterfaces().contains(cx.getText())) {
                                        cx.setSelected(true);
                                    }
                                }
                            }
                            workspace.getInterfaceCB().setText(newClass.getInterfaces().get(0));

                        } else {
                            for (MenuItem x : workspace.getInterfaceCB().getItems()) {
                                if (x instanceof CheckMenuItem) {
                                    CheckMenuItem cx = (CheckMenuItem) x;
                                    cx.setSelected(false);
                                }
                            }
                            workspace.getInterfaceCB().setText("");
                        }
                    }
                }
            }
        });

    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
