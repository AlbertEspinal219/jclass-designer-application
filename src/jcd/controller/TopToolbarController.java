/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jcd.data.Argument;
import jcd.data.DataManager;
import jcd.data.JcdClassPane;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.data.JcdClass;
import jcd.gui.Workspace;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static saf.settings.AppStartupConstants.PATH_WORK;

/**
 *
 * @author Albert
 */
public class TopToolbarController {

    AppTemplate app;

    ArrayList<String> rtPackages;
    ArrayList<String> fxPackages;

    public TopToolbarController(AppTemplate initApp) throws IOException {
        app = initApp;
        initializePackageLists();
    }

    public void initializePackageLists() throws FileNotFoundException, IOException {
        fxPackages = new ArrayList<String>();
        //ZipInputStream fxZip = new ZipInputStream(new FileInputStream(System.getProperty("java.home") + "/lib/ext/jfxrt.jar"));
        ZipInputStream fxZip = new ZipInputStream(new FileInputStream("./jfxrt.jar"));
        for (ZipEntry entry = fxZip.getNextEntry(); entry != null; entry = fxZip.getNextEntry()) {
            if (entry.getName().endsWith(".class") && !entry.getName().contains("$") && !entry.getName().contains("com") && !entry.getName().contains("awt") && !entry.getName().contains("sql")) {
                String packageName = entry.getName().replace('/', '.');
                fxPackages.add(packageName.substring(0, packageName.length() - 6));
            }
        }

        rtPackages = new ArrayList<String>();
        //ZipInputStream rtZip = new ZipInputStream(new FileInputStream(System.getProperty("java.home") + "/lib/rt.jar"));
        ZipInputStream rtZip = new ZipInputStream(new FileInputStream("./rt.jar"));
        for (ZipEntry entry = rtZip.getNextEntry(); entry != null; entry = rtZip.getNextEntry()) {
            if (entry.getName().endsWith(".class") && !entry.getName().contains("$") && !entry.getName().contains("com") && !entry.getName().contains("sun") && !entry.getName().contains("org") && !entry.getName().contains("awt") && !entry.getName().contains("sql")) {
                String packageName = entry.getName().replace('/', '.');
                rtPackages.add(packageName.substring(0, packageName.length() - 6));
            }
        }

    }

    public void clearDirectory(File dirToClear) {
        for (File file : dirToClear.listFiles()) {
            if (file.isDirectory()) {
                clearDirectory(file);
            }
            file.delete();
        }
    }

    public void processExportCode() throws IOException, ClassNotFoundException {
        //First select a directory...

        DataManager dataManager = (DataManager) app.getDataComponent();

        File initialDirectory = new File("./exportProject/exportProject/src");

        //initialDirectory
        try {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(initialDirectory);
        File selectedDirectory = directoryChooser.showDialog(app.getGUI().getWindow());

        clearDirectory(selectedDirectory);

        String directoryPath = selectedDirectory.getAbsolutePath();
        ArrayList<JcdClass> classes = dataManager.getClasses();

        if (classes.size() > 0) {
            for (JcdClass currentClass : classes) {
                ArrayList<String> toImport = new ArrayList();
                File packageName;
                String packagePath = "";
                boolean hasPackage = false;
                File classFile;
                
                
                if (currentClass.getParent() != null || !currentClass.getParent().equals("")) {
                    for (String currentPackage : rtPackages) {
                        if (currentPackage.endsWith("." + currentClass.getParent())) {
                            if (!toImport.contains(currentPackage)) {
                                toImport.add(currentPackage);

                            }
                        }
                    }
                    for (String currentPackage : fxPackages) {
                        if (currentPackage.endsWith("." + currentClass.getParent())) {
                            if (!toImport.contains(currentPackage)) {
                                toImport.add(currentPackage);
                            }
                        }
                    }
                }

                if (currentClass.getPackageName().contains(".")) {
                    hasPackage = true;
                    String nestedPath = currentClass.getPackageName().replace(".", "/");
                    nestedPath = directoryPath + "/" + nestedPath;

                    packageName = new File(nestedPath);
                    packageName.mkdirs();

                } else {
                    hasPackage = true;
                    packageName = new File(directoryPath + "/" + currentClass.getPackageName());
                    packageName.mkdir();
                }
                packagePath = packageName.getAbsolutePath(); 

                ArrayList<Variable> variables = currentClass.getVariables();
                ArrayList<Method> methods = currentClass.getMethods();

                for (Variable v : variables) {
                    for (JcdClass currentClassCheck : classes) {
                        if (v.getType().equals(currentClassCheck.getClassName())) {
                            String importAdd = currentClassCheck.getPackageName();
                            if (!importAdd.equals(currentClass.getPackageName())) {
                                importAdd += "." + currentClassCheck.getClassName();
                                toImport.add(importAdd);
                            }
                        }
                    }
                }
                 for(Method m: methods) {
                    for(JcdClass currentClassCheck: classes) {
                        for (Argument a : m.getArguments()) {
                            if (a.getArgType().equals(currentClassCheck.getClassName())) {
                            String importAdd = currentClassCheck.getPackageName();
                            if (!importAdd.equals(currentClass.getPackageName())) {
                                importAdd += "." + currentClassCheck.getClassName();
                                toImport.add(importAdd);
                        }
                    }
                }
                    }
                }
                
                for(Method m: methods) {
                    for(JcdClass currentClassCheck: classes) {
                            if (m.getReturnType().equals(currentClassCheck.getClassName())) {
                            String importAdd = currentClassCheck.getPackageName();
                            if (!importAdd.equals(currentClass.getPackageName())) {
                                importAdd += "." + currentClassCheck.getClassName();
                                toImport.add(importAdd);
                        }
                    }
                }
                }
                    
                
                // Do above for methods.....

                populateImportList(variables, methods, toImport);

                if (hasPackage) {

                    classFile = new File(packagePath, currentClass.getClassName() + ".java");

                } else {

                    classFile = new File(directoryPath, currentClass.getClassName() + ".java");
                }

                classFile.createNewFile();

                PrintWriter out = new PrintWriter(classFile);

                if (currentClass.getPackageName() != null && !currentClass.getPackageName().equals("")) {
                    out.print("package " + currentClass.getPackageName() + ";");
                    out.print("\n");
                }
                for (String importString : toImport) {
                    out.print("import " + importString + ";");
                    out.print("\n");
                }
                out.print("\n");
                
                out.print("public ");
                
                if (currentClass.isIsAbstract()) {
                    out.print("abstract ");
                }
                if (currentClass.isIsInterface()) {
                    out.print("interface " + currentClass.getClassName());
                }
                else {
                    out.print("class " + currentClass.getClassName());
                }
                if (currentClass.getParent() != null && !currentClass.getParent().equals("")) {
                    out.print(" extends " + currentClass.getParent());
                }
                
                if (currentClass.getInterfaces().size() > 0) {
                    out.print(" implements ");
                for(int i = 0; i < currentClass.getInterfaces().size(); i++) {
                    if(i != currentClass.getInterfaces().size()-1) {
                        out.print(currentClass.getInterfaces().get(i) +",");
                    }
                    else {
                        out.print(currentClass.getInterfaces().get(i));
                    }
                }
                }

                out.print(" {");

                out.print("\n");

                for (Variable v : variables) {
                    out.print("\t" + v.exportString());
                    out.print("\n");
                }

                for (Method m : methods) {
                    out.print("\t" + m.exportString(currentClass.getClassName()));
                    out.print("\n");
                }

                out.println("\n");
                out.println("}");
                out.close();

            }
        }
        }
        catch (NullPointerException e) {
        }
    }

    public void populateImportList(ArrayList<Variable> variables, ArrayList<Method> methods, ArrayList<String> toImport) {
        for (Variable v : variables) {
            for (String currentPackage : rtPackages) {
                if (currentPackage.endsWith("." + v.getType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);

                    }
                }
            }
            for (String currentPackage : fxPackages) {
                if (currentPackage.endsWith("." + v.getType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);
                    }
                }
            }
        }

        for (Method m : methods) {
            for (String currentPackage : rtPackages) {
                
                for(int i = 0; i < m.getArguments().size(); i++) {
                if (currentPackage.endsWith("." + m.getArg(i).getArgType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);
                    }
                }
            }
            }
            for (String currentPackage : fxPackages) {
                for(int i = 0; i < m.getArguments().size(); i++) {
                if (currentPackage.endsWith("." + m.getArg(i).getArgType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);
                    }
                }
            }
                }

            for (String currentPackage : rtPackages) {
                if (currentPackage.endsWith("." + m.getReturnType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);
                    }
                }
            }

            for (String currentPackage : fxPackages) {
                if (currentPackage.endsWith("." + m.getReturnType())) {
                    if (!toImport.contains(currentPackage)) {
                        toImport.add(currentPackage);
                    }
                }
            }

        }
    }

    public void processSelectSelectionTool() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        // CHANGE THE CURSOR
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);
        workspace.setSelectingStateTrue();
    }

    public void processAddClass() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        JcdClass newClass = new JcdClass();
        JcdClassPane newClassPane = new JcdClassPane();
        newClassPane.setAssociatedClass(newClass);
        newClass.setClassPane(newClassPane);

        newClassPane.getClassTA().setText(newClass.getClassName());

        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        workspace.setSelectingStateFalse();
        dataManager.getClasses().add(newClass);
        dataManager.addClassPane(newClassPane);
        dataManager.getClassNames().add(newClass.getClassName());
        workspace.getVarTable().setItems(null);
        workspace.getMethTable().setItems(null);
        if (dataManager.getselectedClass() != null) {
            dataManager.unhighlightClass(dataManager.getselectedClass().getClassPane());
            workspace.getClassNameTF().clear();
            workspace.getPackageNameTF().clear();
            workspace.getVarTable().setItems(null);
            workspace.getMethTable().setItems(null);
            workspace.getParentCB().setText("");
            workspace.getExternalParentTF().setText(newClass.getExternalParent());
            workspace.getInterfaceCB().setText("");
            
            for(MenuItem x : workspace.getInterfaceCB().getItems()) {
                            if (x instanceof CheckMenuItem) {
                            CheckMenuItem cx = (CheckMenuItem) x;
                            if (!newClassPane.getAssociatedClass().getInterfaces().contains(cx.getText())) {
                                cx.setSelected(false);
                            }
                        }
            }
        }

        dataManager.setSelectedClass(newClass);
        dataManager.highlightClass(newClass.getClassPane());
        workspace.getClassNameTF().setPromptText(newClass.getClassName());
        workspace.getPackageNameTF().setPromptText(newClass.getPackageName());
        workspace.getParentCB().setText(newClass.getParent());
        workspace.getExternalParentTF().setText(newClass.getExternalParent());
        
        MenuItem newItem = new MenuItem(newClass.getClassName());
        newItem.setOnAction(e -> {
           if (newItem.getText().equals(newClass.getParent())) {
                newClass.setParent(null);
                workspace.getParentCB().setText(null);
            }
           else if(newItem.getText() != null && !newItem.getText().equals("")) {
                if (newItem.getText().equals(dataManager.getselectedClass().getClassName())) {
                    System.out.println("Self cant be parent... alert box later");
                }
                if (newItem.getText().equals(dataManager.getselectedClass().getParent())) {
                    System.out.println("remove noticed...");
                    JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
                    dataManager.getClassPanes().remove(selectedPane.getParentLine());
                    dataManager.getselectedClass().setParent("");
                    workspace.getParentCB().setText("");
                    
                }
                else {
                dataManager.getselectedClass().setParent(newItem.getText());
                parentPickedInternal(dataManager.getselectedClass().getClassPane(), dataManager.getselectedClass().getParent());
                workspace.getParentCB().setText(newItem.getText());
                }
            }
        });
        workspace.getParentCB().getItems().add(newItem);
        
        workspace.getVarTable().setItems(FXCollections.observableList(newClassPane.getAssociatedClass().getVariables()));
        workspace.getMethTable().setItems(FXCollections.observableList(newClassPane.getAssociatedClass().getMethods()));
        makeDraggable(newClassPane);

        Rectangle aggregation = new Rectangle();
        aggregation.setWidth(25);
        aggregation.setHeight(25);
        aggregation.getTransforms().add(new Rotate(45));
        
        aggregation.layoutXProperty().bind(newClassPane.layoutXProperty().subtract(20));
        aggregation.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.prefHeightProperty().divide(2).subtract(10)));
        
        
        workspace.getDesignPane().getChildren().add(aggregation);
        newClassPane.setAggregationConnecter(aggregation);
        
        
        Polygon inheritence = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });
        
        inheritence.layoutXProperty().bind(newClassPane.layoutXProperty().add(newClassPane.prefWidthProperty().divide(2).subtract(22.5)));
     
        
        inheritence.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.heightProperty().subtract(5)));
        
        newClassPane.setInheritanceconnector(inheritence);
        
        
        
        workspace.getDesignPane().getChildren().add(inheritence);
        newClassPane.setInheritanceconnector(inheritence);
        
        Polygon uses = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });
        uses.getTransforms().add(new Rotate(-90));
        
        uses.layoutXProperty().bind(newClassPane.layoutXProperty().add(newClassPane.prefWidthProperty()));
     
        
        uses.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.prefHeightProperty().subtract(0)));
      
        
        workspace.getDesignPane().getChildren().add(uses);
        newClassPane.setUsesConnector(uses);
            
        makeSelectable(newClassPane);

    }
        public void processAddInterface() {
        
        DataManager dataManager = (DataManager) app.getDataComponent();
        JcdClass newClass = new JcdClass();
        newClass.setIsInterface(true);
        JcdClassPane newClassPane = new JcdClassPane();
        newClassPane.setAssociatedClass(newClass);
        newClass.setClassPane(newClassPane);

        newClassPane.getClassTA().setText(newClass.getClassName() + "<<interface>>");

        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        workspace.setSelectingStateFalse();
        
        dataManager.getClasses().add(newClass);
        dataManager.addClassPane(newClassPane);
        
        workspace.getVarTable().setItems(null);
        workspace.getMethTable().setItems(null);
        
        if (dataManager.getselectedClass() != null) {
            dataManager.unhighlightClass(dataManager.getselectedClass().getClassPane());
            workspace.getClassNameTF().clear();
            workspace.getPackageNameTF().clear();
            workspace.getVarTable().setItems(null);
            workspace.getMethTable().setItems(null);
            workspace.getInterfaceCB().setText("");
        }

        dataManager.setSelectedClass(newClass);
        dataManager.highlightClass(newClass.getClassPane());
        workspace.getClassNameTF().setPromptText(newClass.getClassName());
        workspace.getPackageNameTF().setPromptText(newClass.getPackageName());
        if (newClass.getInterfaces().size() > 0) 
            workspace.getInterfaceCB().setText(newClass.getInterfaces().get(0));
        
        
        
        CheckMenuItem newItem = new CheckMenuItem(newClass.getClassName());
        
        newItem.setOnAction(e -> {
            if (dataManager.getselectedClass() != null) {
           if(newItem.getText() != null && !newItem.getText().equals("")) {
                if (newItem.getText().equals(dataManager.getselectedClass().getClassName())) {
                    System.out.println("Self cant be interface... alert box later");
                }
                if (dataManager.getselectedClass().getInterfaces().contains(newItem.getText())) {
                        System.out.println("remove noticed");
                        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
                        int index = dataManager.getselectedClass().getInterfaces().indexOf(newItem.getText());
                        dataManager.getClassPanes().remove(selectedPane.getInterfaceLines().get(index));
                        selectedPane.getInterfaceLines().remove(dataManager.getselectedClass().getInterfaces().indexOf(newItem.getText()));
                        dataManager.getselectedClass().getInterfaces().remove(newItem.getText());
                        if(dataManager.getselectedClass().getInterfaces().size() > 0) {
                        workspace.getInterfaceCB().setText(dataManager.getselectedClass().getInterfaces().get(0));
                        }
                        else {
                            workspace.getInterfaceCB().setText(null);
                        }
                        
                }
                else {
                dataManager.getselectedClass().getInterfaces().add(newItem.getText());
                interfacePickedInternal(dataManager.getselectedClass().getClassPane(), newItem.getText());
                workspace.getInterfaceCB().setText(newItem.getText());
                }
            }
            }
        });
        workspace.getInterfaceCB().getItems().add(newItem);
        
        workspace.getVarTable().setItems(FXCollections.observableList(newClassPane.getAssociatedClass().getVariables()));
        workspace.getMethTable().setItems(FXCollections.observableList(newClassPane.getAssociatedClass().getMethods()));
        makeDraggable(newClassPane);

        Rectangle aggregation = new Rectangle();
        aggregation.setWidth(25);
        aggregation.setHeight(25);
        aggregation.getTransforms().add(new Rotate(45));
        
        aggregation.layoutXProperty().bind(newClassPane.layoutXProperty().subtract(20));
        aggregation.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.prefHeightProperty().divide(2).subtract(10)));
        
        
        workspace.getDesignPane().getChildren().add(aggregation);
        newClassPane.setAggregationConnecter(aggregation);
        
        
        Polygon inheritence = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });
        
        inheritence.layoutXProperty().bind(newClassPane.layoutXProperty().add(newClassPane.prefWidthProperty().divide(2).subtract(22.5)));
     
        
        inheritence.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.prefHeightProperty().subtract(5)));
        
        newClassPane.setInheritanceconnector(inheritence);
        
        
        
        workspace.getDesignPane().getChildren().add(inheritence);
        newClassPane.setInheritanceconnector(inheritence);
        
        Polygon uses = new Polygon(new double[] { 22.5, 5, 5, 40, 40, 40, });
        uses.getTransforms().add(new Rotate(-90));
        
        uses.layoutXProperty().bind(newClassPane.layoutXProperty().add(newClassPane.prefWidthProperty()));
     
        
        uses.layoutYProperty().bind(newClassPane.layoutYProperty().add(newClassPane.prefHeightProperty().subtract(0)));
      
        
        workspace.getDesignPane().getChildren().add(uses);
        newClassPane.setUsesConnector(uses);
            
        makeSelectable(newClassPane);

    }
    
    
    public void processRemove() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        if (dataManager.getselectedClass() != null) {
        JcdClass selectedClass = dataManager.getselectedClass();
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        
        String paneName = selectedPane.getClassTA().getText();
        if(selectedClass.isIsInterface()) {
            for(JcdClass currentClass : dataManager.getClasses()) {
                currentClass.getInterfaces().remove(selectedClass.getClassName());
            }
        }
        
        for(JcdClass currentClass : dataManager.getClasses()) {
            if(paneName.equals(currentClass.getParent())) {
                currentClass.setParent("");
                currentClass.setExternalParent("");
            }
            
            if(paneName.equals(currentClass.getExternalParent())) {
                currentClass.setParent("");
                currentClass.setExternalParent("");
            }
        }
        
        if(paneName.contains("<<")) {
            paneName = paneName.replace("<<interface>>", "");
        }
        
        for(JcdClass currentClass : dataManager.getClasses()) {
            if(currentClass.getInterfaces().contains(paneName)) {
                currentClass.getInterfaces().remove(paneName);
                 workspace.getInterfaceCB().setText("");
                 workspace.getExternalInterfaceTF().setText("");
            }
        }
        ArrayList<MenuItem> items = new ArrayList();
        
        for(MenuItem x: workspace.getInterfaceCB().getItems()) {
            items.add(x);
        }
        
        for(MenuItem x  : items) {
            if (x.getText().equals(selectedClass.getClassName())) {
                workspace.getInterfaceCB().getItems().remove(x);
            }
        }
        dataManager.getClasses().remove(dataManager.getselectedClass());
        
        dataManager.getClassPanes().remove(selectedPane.getUsesConnector());
        dataManager.getClassPanes().remove(selectedPane.getInheritanceconnector());
        dataManager.getClassPanes().remove(selectedPane.getAggregationConnecter());
        dataManager.getClassPanes().remove(selectedPane.getParentLine());
        
        if (selectedPane.getInterfaceLines().size() > 0) {
            for(Line x: selectedPane.getInterfaceLines()) {
                dataManager.getClassPanes().remove(x);
            }
        }
        
        if (selectedPane.getAggregateLines().size() > 0) {
            for(Line x: selectedPane.getAggregateLines()) {
                dataManager.getClassPanes().remove(x);
            }
        }
        
        if (selectedPane.getUsesLines().size() > 0) {
            for(Line x: selectedPane.getUsesLines()) {
                dataManager.getClassPanes().remove(x);
            }
        }
        if(selectedPane.getChildClassPane() != null) {
            dataManager.getClassPanes().remove(selectedPane.getChildClassPane().getParentLine());
        }
        
        
        dataManager.getClassPanes().remove(dataManager.getselectedClass().getClassPane());
        workspace.getClassNameTF().setPromptText("");
        workspace.getPackageNameTF().setPromptText("");
        workspace.getClassNameTF().clear();
        workspace.getPackageNameTF().clear();
        workspace.getVarTable().setItems(null);
        workspace.getParentCB().getItems().clear();
        workspace.getParentCB().setText("");
        workspace.getExternalParentTF().setText("");
        
        for(int i =0; i < dataManager.getClasses().size(); i++) {
            if (dataManager.getClasses().get(i).isIsInterface() == false) {
            MenuItem newItem = new MenuItem(dataManager.getClasses().get(i).getClassName());
            
            newItem.setOnAction(e -> {
       
           if (newItem.getText().equals(dataManager.getselectedClass().getParent())) {
                dataManager.getselectedClass().setParent(null);
                workspace.getParentCB().setText(null);
            }
           else if(newItem.getText() != null && !newItem.getText().equals("")) {
                if (newItem.getText().equals(dataManager.getselectedClass().getClassName())) {
                    System.out.println("Self cant be parent... alert box later...");
                }
                else {
                dataManager.getselectedClass().setParent(newItem.getText());
                parentPickedInternal(dataManager.getselectedClass().getClassPane(), dataManager.getselectedClass().getParent());
                workspace.getParentCB().setText(newItem.getText());
                }
            }
        });
            workspace.getParentCB().getItems().add(newItem);
        }
        }
    }
    }

    public void processNameEditing() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        if (dataManager.getselectedClass() != null) {
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        selectedPane.getClassTA().setText(workspace.getClassNameTF().getText());
        if (dataManager.getselectedClass().isIsInterface()) {
            selectedPane.getClassTA().setText(workspace.getClassNameTF().getText() + "<<interface>>");
        }
        selectedPane.getAssociatedClass().setClassName(workspace.getClassNameTF().getText());
        
        if (!dataManager.getselectedClass().isIsInterface()) {
        workspace.getParentCB().getItems().clear();
        
        for(int i =0; i < dataManager.getClasses().size(); i++) {
            if(dataManager.getClasses().get(i).isIsInterface() == false) {
            MenuItem newItem = new MenuItem(dataManager.getClasses().get(i).getClassName());
            
            newItem.setOnAction(e -> {
       
           if (newItem.getText().equals(dataManager.getselectedClass().getParent())) {
                dataManager.getselectedClass().setParent(null);
                workspace.getParentCB().setText(null);
            }
           else if(newItem.getText() != null && !newItem.getText().equals("")) {
                if (newItem.getText().equals(dataManager.getselectedClass().getClassName())) {
                    System.out.println("Self cant be parent... alert box later...");
                }
                else {
                dataManager.getselectedClass().setParent(newItem.getText());
                parentPickedInternal(dataManager.getselectedClass().getClassPane(), dataManager.getselectedClass().getParent());
                workspace.getParentCB().setText(newItem.getText());
                }
            }
        });
                       workspace.getParentCB().getItems().add(newItem);
        }
        }
        //Interface Name Editing....
        }
        else {
            workspace.getInterfaceCB().getItems().clear();
            
            for(int i =0; i < dataManager.getClasses().size(); i++) {
            
                if (dataManager.getClasses().get(i).isIsInterface()) {
                    CheckMenuItem newItem = new CheckMenuItem(dataManager.getClasses().get(i).getClassName());
            
            newItem.setOnAction(e -> {
       
           if(newItem.getText() != null && !newItem.getText().equals("")) {
                if (newItem.getText().equals(dataManager.getselectedClass().getClassName())) {
                    System.out.println("Self cant be interface... alert box later...");
                }
                else {
                dataManager.getselectedClass().getInterfaces().add(newItem.getText());
                interfacePickedInternal(dataManager.getselectedClass().getClassPane(), newItem.getText());
                workspace.getInterfaceCB().setText(newItem.getText());
                }
            }
        });
                       workspace.getInterfaceCB().getItems().add(newItem);
        }
            }
        }
        
        }
    }

    public void processPackageEditing() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) (app.getWorkspaceComponent());
        if (dataManager.getselectedClass() != null) {
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        selectedPane.getPackageTA().setText(workspace.getPackageNameTF().getText());
        selectedPane.getAssociatedClass().setPackageName(workspace.getPackageNameTF().getText());
        }
    }

    public void makeSelectable(JcdClassPane classPane) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        JcdClass newClass = classPane.getAssociatedClass();
        classPane.setOnMouseClicked((MouseEvent e) -> {
            if (workspace.isSelectingState()) {
                int ex = (int) e.getX();
                int ey = (int) e.getY();
                
                if (app.getGUI().getSnapCB().isSelected()) {
                    classPane.setLayoutX(((int)classPane.getLayoutX()+25) / 50 * 50);
                    classPane.setLayoutY(((int)classPane.getLayoutY()+25) / 50 * 50);
                }
                if (! (((ex > classPane.getPackagePane().getPrefWidth()) && ex < classPane.getPrefWidth()) && (ey < classPane.getPackagePane().getPrefHeight())) ) {
                    if (dataManager.getselectedClass() != null) {
                        dataManager.unhighlightClass(dataManager.getselectedClass().getClassPane());
                    }
                    dataManager.setSelectedClass(newClass);
                    dataManager.highlightClass(classPane);
                    if (newClass != null) {
                    workspace.getVarTable().setItems(FXCollections.observableList(classPane.getAssociatedClass().getVariables()));
                    workspace.getMethTable().setItems(FXCollections.observableList(classPane.getAssociatedClass().getMethods()));
                    workspace.getClassNameTF().setPromptText(newClass.getClassName());
                    workspace.getPackageNameTF().setPromptText(newClass.getPackageName());
                    workspace.getParentCB().setText(classPane.getAssociatedClass().getParent());
                    workspace.getExternalParentTF().setText(newClass.getExternalParent());
                    
                    if (newClass.getInterfaces().size() > 0) {
                        for(MenuItem x : workspace.getInterfaceCB().getItems()) {
                            if (x instanceof CheckMenuItem) {
                            CheckMenuItem cx = (CheckMenuItem) x;
                            if (classPane.getAssociatedClass().getInterfaces().contains(cx.getText())) {
                                cx.setSelected(true);
                            }
                        }
                        }
                    workspace.getInterfaceCB().setText(newClass.getInterfaces().get(0));
                    
                    }
                    else {
                        for(MenuItem x : workspace.getInterfaceCB().getItems()) {
                            if (x instanceof CheckMenuItem) {
                            CheckMenuItem cx = (CheckMenuItem) x;
                            cx.setSelected(false);
                            }
                        }
                        workspace.getInterfaceCB().setText("");
                    }
                    }
                }
            }
            });
        
    }
    public void makeSelectableAPI(JcdClassPane classPane) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        JcdClass newClass = new JcdClass();
        classPane.setAssociatedClass(newClass);
        newClass.setClassPane(classPane);
        classPane.setOnMouseClicked(e -> {
            if (workspace.isSelectingState()) {
                int ex = (int) e.getX();
                int ey = (int) e.getY();
                if (app.getGUI().getSnapCB().isSelected()) {
                    classPane.setLayoutX(((int)classPane.getLayoutX()+25) / 50 * 50);
                    classPane.setLayoutY(((int)classPane.getLayoutY()+25) / 50 * 50);
                }
                if (! (((ex > classPane.getPackagePane().getPrefWidth()) && ex < classPane.getPrefWidth()) && (ey < classPane.getPackagePane().getPrefHeight())) ) {
                    if (dataManager.getselectedClass() != null) {
                        dataManager.unhighlightClass(dataManager.getselectedClass().getClassPane());
                    }
                    dataManager.setSelectedClass(newClass);
                    dataManager.highlightClass(classPane);
                }
            }
            });
    }
    public void makeDraggable(JcdClassPane classPane) {

        classPane.setOnMousePressed(e -> {

            Workspace workspace = (Workspace) (app.getWorkspaceComponent());
            boolean dragState = workspace.isSelectingState();
            if (dragState) {

                classPane.setMouseX(e.getSceneX());
                classPane.setMouseY(e.getSceneY());
                
                classPane.setX(classPane.getLayoutX());
                classPane.setY(classPane.getLayoutY());
            }
        });

        classPane.setOnMouseDragged(e -> {
            Workspace workspace = (Workspace) (app.getWorkspaceComponent());
            boolean dragState = workspace.isSelectingState();
            boolean resizeState = workspace.isResizingState();
            if (dragState) {

                double xDiff = e.getSceneX() - classPane.getMouseX();
                double yDiff = e.getSceneY() - classPane.getMouseY();      
                
                if (resizeState) {
                //classPane.getPackagePane().setPrefWidth( (classPane.getPrefWidth() + xDiff) / 5);
                classPane.setPrefHeight(classPane.getPrefHeight()+ yDiff );
                classPane.setPrefWidth(classPane.getPrefWidth() + xDiff);
                }
                
                classPane.setX(classPane.getX() + xDiff);
                classPane.setY(classPane.getY() + yDiff);
                
                double X = classPane.getX();
                double Y = classPane.getY();
                
                if (resizeState == false) {
                classPane.setLayoutX(X);
                classPane.setLayoutY(Y);
                }

                classPane.setMouseX(e.getSceneX());
                classPane.setMouseY(e.getSceneY());
                
                e.consume();
            }
        });
    }
    
    public void parentPickedInternal(JcdClassPane classPane, String parent) {
        //System.out.println(classPane.getAssociatedClass().getClassName());
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        if (dataManager.getselectedClass() != null) {
        
        //External
          if (!dataManager.getselectedClass().getParent().equals(null) &&
                !dataManager.getselectedClass().getParent().equals("")) {
                ObservableList<Node> classPanes = dataManager.getClassPanes();
                
                for(Node n : classPanes) {
                    if (n instanceof JcdClassPane) {
                        JcdClassPane pane = (JcdClassPane) n;
                        
                        if (pane.isExternal()) {
                            dataManager.getClassPanes().remove(n);
                            dataManager.getClassPanes().remove(((JcdClassPane) n).getParentLine());
                            dataManager.getselectedClass().setExternalParent("");
                            workspace.getExternalParentTF().setText("");
                            break;
                        }
                    }
                }
        }
          
       //Internal   
       
       ObservableList<Node> nodes = dataManager.getClassPanes();
        
        ArrayList<JcdClassPane> classPanes = new ArrayList();
        
        for(Node n: nodes) {
            if (n instanceof JcdClassPane) {
                classPanes.add((JcdClassPane)n);
            }
        }
        for (JcdClassPane pane : classPanes) { 
            if (dataManager.getselectedClass().getClassPane().equals(pane)) {
                if(pane.getParentLine() != null) {
                    dataManager.getClassPanes().remove(pane.getParentLine());
                    
                }
            }
            }
        
        
        Line line = new Line();
    
        
        ArrayList<JcdClass> classes = dataManager.getClasses();
        
        JcdClass parentClass = null;
        
        for(JcdClass currentClass: classes) {
            if (currentClass.getClassName().equals(parent)) {
                parentClass = currentClass;
                break;
            }
        }
        JcdClassPane parentPane = null;
        if (parentClass != null) {
        parentPane = parentClass.getClassPane();
        }
        
        line.startXProperty().bind(classPane.layoutXProperty().add(classPane.prefWidthProperty().divide(2)));
        line.startYProperty().bind(classPane.layoutYProperty().add(classPane.translateYProperty().add(classPane.getPackagePane().prefHeightProperty())));
        
        if(parentClass != null) {
          
             line.endXProperty().bind(parentPane.getInheritanceconnector().layoutXProperty().add(22.5));
             line.endYProperty().bind(parentPane.getInheritanceconnector().layoutYProperty().add(40));
        }
        
        classPane.setParentLine(line);
        classPane.setParentClassPane(parentPane);
        if (parentPane != null) {
            parentPane.setChildClassPane(classPane);
        }
        workspace.getDesignPane().getChildren().add(line);
        }
    }
    
    public void processExternalParent() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        if (dataManager.getselectedClass() != null) {
        
       // External
       
       JcdClass previousAPIClass = null;
        
        if (!dataManager.getselectedClass().getParent().equals(null) &&
                !dataManager.getselectedClass().getParent().equals("")) {
                ObservableList<Node> classPanes = dataManager.getClassPanes();
                
                for(Node n : classPanes) {
                    if (n instanceof JcdClassPane) {
                        JcdClassPane pane = (JcdClassPane) n;
                        
                        if (pane.isExternal()) {
                            dataManager.getClassPanes().remove(n);
                            dataManager.getClassPanes().remove(((JcdClassPane) n).getParentLine());
                            break;
                        }
                    }
                }
        }
        
        //Internal
        ObservableList<Node> nodes = dataManager.getClassPanes();
        
        ArrayList<JcdClassPane> classPanes = new ArrayList();
        
        for(Node n: nodes) {
            if (n instanceof JcdClassPane) {
                classPanes.add((JcdClassPane)n);
            }
        }
        for (JcdClassPane pane : classPanes) { 
            if (dataManager.getselectedClass().getClassPane().equals(pane)) {
                if(pane.getParentLine() != null) {
                    dataManager.getClassPanes().remove(pane.getParentLine());
                    
                }
            }
            }
        Line line = new Line();
        
        JcdClassPane parentAPIPane = new JcdClassPane();
        dataManager.addExternalPane(parentAPIPane);
        parentAPIPane.setExternal(true);
        
        parentAPIPane.getChildren().remove(parentAPIPane.getPackagePane());
        parentAPIPane.getChildren().remove(parentAPIPane.getVariablesPane());
        parentAPIPane.getChildren().remove(parentAPIPane.getMethodsPane());
        
        parentAPIPane.getClassTA().setText(workspace.getExternalParentTF().getText());
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        
        dataManager.getselectedClass().setExternalParent(workspace.getExternalParentTF().getText());
        dataManager.getselectedClass().setParent(workspace.getExternalParentTF().getText());
        
        workspace.getParentCB().setText(dataManager.getselectedClass().getParent());
        //parentAPIPane.setLayoutX( (selectedPane.getLayoutX()) + (selectedPane.getPrefWidth()/2));
        parentAPIPane.setLayoutY( selectedPane.getLayoutY() - 100);
        makeSelectableAPI(parentAPIPane);
        makeDraggable(parentAPIPane);
        
        parentAPIPane.getClassPane().prefWidthProperty().unbind();
        parentAPIPane.getClassPane().prefHeightProperty().unbind();
        
        parentAPIPane.getClassPane().setPrefWidth(100);
        parentAPIPane.getClassPane().setPrefHeight(40);
        parentAPIPane.setPrefWidth(100);
        parentAPIPane.setPrefHeight(40);
        
        parentAPIPane.setLayoutX( ((selectedPane.getLayoutX()) + (selectedPane.getPrefWidth()/2)) - parentAPIPane.getPrefWidth()/2);
        line.startXProperty().bind(selectedPane.layoutXProperty().add(selectedPane.prefWidthProperty().divide(2)));
        line.startYProperty().bind(selectedPane.layoutYProperty().add(selectedPane.translateYProperty().add(selectedPane.getPackagePane().prefHeightProperty())));
        
        line.endXProperty().bind(parentAPIPane.layoutXProperty().add(parentAPIPane.getClassPane().prefWidthProperty().divide(2)));
        line.endYProperty().bind(parentAPIPane.layoutYProperty().add(parentAPIPane.getClassPane().prefHeightProperty()));
        
        parentAPIPane.setParentLine(line);
        selectedPane.setParentLine(line);
        parentAPIPane.setChildClassPane(selectedPane);
        workspace.getDesignPane().getChildren().add(parentAPIPane);
        workspace.getDesignPane().getChildren().add(line);
         
        }
    }
    
    public void interfacePickedInternal(JcdClassPane classPane, String interfaceName) {
    //System.out.println(classPane.getAssociatedClass().getClassName());
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        if (dataManager.getselectedClass() != null) {
        
        Line line = new Line();
    
        line.getStrokeDashArray().addAll(2d, 11d);
        ArrayList<JcdClass> classes = dataManager.getClasses();
        
        JcdClass interfaceClass = null;
        for(JcdClass currentClass: classes) {
            if (currentClass.getClassName().equals(interfaceName)) {
                if(currentClass.isIsInterface())  {
                    interfaceClass = currentClass;
                    break;
                }
            }
        }
        
        JcdClassPane interfacePane = null;
        if (interfaceClass != null) {
        interfacePane = interfaceClass.getClassPane();
        }
       
        line.startXProperty().bind(classPane.layoutXProperty().add(classPane.prefWidthProperty().divide(2)));
        line.startYProperty().bind(classPane.layoutYProperty().add(classPane.translateYProperty().add(classPane.getPackagePane().prefHeightProperty())));
        
       
        if(interfaceClass != null) {
             line.endXProperty().bind(interfacePane.getInheritanceconnector().layoutXProperty().add(22.5));
             line.endYProperty().bind(interfacePane.getInheritanceconnector().layoutYProperty().add(40));
        }
        
        interfacePane.getInterfaceLines().add(line);
        classPane.getInterfaceLines().add(line);
        if (interfacePane != null) {
            classPane.getInterfacePanes().add(interfacePane);
        }
        workspace.getDesignPane().getChildren().add(line);
        }
    }
    
    public void processExternalInterface() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        
        if (dataManager.getselectedClass() != null) {
        Line line = new Line();
        line.getStrokeDashArray().addAll(2d, 11d);
        
        JcdClassPane interfaceAPIPane = new JcdClassPane();
        //interfaceAPIPane.setExternal(true);
        
        interfaceAPIPane.getChildren().remove(interfaceAPIPane.getPackagePane());
        interfaceAPIPane.getChildren().remove(interfaceAPIPane.getVariablesPane());
        interfaceAPIPane.getChildren().remove(interfaceAPIPane.getMethodsPane());
        
        interfaceAPIPane.getClassTA().setText(workspace.getExternalInterfaceTF().getText());
        interfaceAPIPane.getClassTA().setText(workspace.getExternalInterfaceTF().getText() + "<<interface>>");
        
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        selectedPane.getInterfacePanes().add(interfaceAPIPane);
        
        interfaceAPIPane.getInterfaceLines().add(line);
        selectedPane.getInterfaceLines().add(line);
        
        dataManager.getselectedClass().getInterfaces().add(workspace.getExternalInterfaceTF().getText());
        
        interfaceAPIPane.setLayoutY( selectedPane.getLayoutY() - 100);
        makeSelectableAPI(interfaceAPIPane);
        makeDraggable(interfaceAPIPane);
        
        interfaceAPIPane.getClassPane().prefWidthProperty().unbind();
        interfaceAPIPane.getClassPane().prefHeightProperty().unbind();
        interfaceAPIPane.getClassPane().setPrefWidth(100);
        interfaceAPIPane.getClassPane().setPrefHeight(40);
        interfaceAPIPane.setPrefWidth(100);
        interfaceAPIPane.setPrefHeight(40);
        
        interfaceAPIPane.setLayoutX( ((selectedPane.getLayoutX()) + (selectedPane.getPrefWidth()/2)) - interfaceAPIPane.getPrefWidth()/2);
        line.startXProperty().bind(selectedPane.layoutXProperty().add(selectedPane.prefWidthProperty().divide(2)));
        line.startYProperty().bind(selectedPane.layoutYProperty().add(selectedPane.translateYProperty().add(selectedPane.getPackagePane().prefHeightProperty())));
        
        line.endXProperty().bind(interfaceAPIPane.layoutXProperty().add(interfaceAPIPane.getClassPane().prefWidthProperty().divide(2)));
        line.endYProperty().bind(interfaceAPIPane.layoutYProperty().add(interfaceAPIPane.getClassPane().prefHeightProperty()));
        
        
        workspace.getDesignPane().getChildren().add(interfaceAPIPane);
        workspace.getDesignPane().getChildren().add(line);
    }
    }
    
    public void processAPIAggregate(String aggregateName) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
        
        if (dataManager.getselectedClass() != null) {
        ArrayList<String> aggregates = dataManager.getselectedClass().getClassPane().getAggregates();
        
        if( aggregates.contains(aggregateName) == false ) {
        
        Line line = new Line();
        
        line.startXProperty().bind(selectedPane.getAggregationConnecter().layoutXProperty().subtract(16));
        line.startYProperty().bind(selectedPane.getAggregationConnecter().layoutYProperty().add(20));
        
        JcdClassPane aggregateAPIPane = new JcdClassPane();
        dataManager.addExternalPane(aggregateAPIPane);
        //aggregateAPIPane.setExternal(true);
        
        aggregateAPIPane.getChildren().remove(aggregateAPIPane.getPackagePane());
        aggregateAPIPane.getChildren().remove(aggregateAPIPane.getVariablesPane());
        aggregateAPIPane.getChildren().remove(aggregateAPIPane.getMethodsPane());
        
        aggregateAPIPane.getClassTA().setText(aggregateName);
        
        
        selectedPane.getAggregatePanes().add(aggregateAPIPane);
        
        selectedPane.getAggregates().add(aggregateName);
        aggregateAPIPane.getAggregateLines().add(line);
        selectedPane.getAggregateLines().add(line);
        
        
       
        aggregateAPIPane.setLayoutY( selectedPane.getLayoutY() - 50);
        makeSelectableAPI(aggregateAPIPane);
        makeDraggable(aggregateAPIPane);
        
        aggregateAPIPane.getClassPane().prefWidthProperty().unbind();
        aggregateAPIPane.getClassPane().prefHeightProperty().unbind();
        aggregateAPIPane.getClassPane().setPrefWidth(100);
        aggregateAPIPane.getClassPane().setPrefHeight(40);
        aggregateAPIPane.setPrefWidth(100);
        aggregateAPIPane.setPrefHeight(40);
        
        
        aggregateAPIPane.setLayoutX( ((selectedPane.getLayoutX()) - (selectedPane.getPrefWidth()/2)) - aggregateAPIPane.getPrefWidth()/2);
        
        
        line.endXProperty().bind(aggregateAPIPane.layoutXProperty().add(aggregateAPIPane.getClassPane().prefWidthProperty().divide(2)));
        line.endYProperty().bind(aggregateAPIPane.layoutYProperty().add(aggregateAPIPane.getClassPane().prefHeightProperty()));
        
        
        workspace.getDesignPane().getChildren().add(aggregateAPIPane);
        workspace.getDesignPane().getChildren().add(line);
        }
        }
    }
    
    public void processAPIUses(String useName) {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        JcdClassPane selectedPane = dataManager.getselectedClass().getClassPane();
                
        if (dataManager.getselectedClass() != null) {
        ArrayList<String> uses = dataManager.getselectedClass().getClassPane().getUses();
        boolean alreadyInDesign = false;
        
        if (uses.contains(useName) == true) {
            alreadyInDesign = true;
        }
        
        Line line = new Line();
        
        line.startXProperty().bind(selectedPane.getUsesConnector().layoutXProperty().add(40));
        line.startYProperty().bind(selectedPane.getUsesConnector().layoutYProperty().subtract(22.5));
       
        line.getStrokeDashArray().addAll(2d, 11d);
        JcdClassPane useAPIPane = new JcdClassPane();
        dataManager.addExternalPane(useAPIPane);
        //useAPIPane.setExternal(true);
        
        useAPIPane.getChildren().remove(useAPIPane.getPackagePane());
        useAPIPane.getChildren().remove(useAPIPane.getVariablesPane());
        useAPIPane.getChildren().remove(useAPIPane.getMethodsPane());
        
        useAPIPane.getClassTA().setText(useName);
        
        selectedPane.getUsesPanes().add(useAPIPane);
        
        selectedPane.getUses().add(useName);
        useAPIPane.getUsesLines().add(line);
        selectedPane.getUsesLines().add(line);
        
        
       
        useAPIPane.setLayoutY( selectedPane.getLayoutY() - 50);
        makeSelectableAPI(useAPIPane);
        makeDraggable(useAPIPane);
        
        useAPIPane.getClassPane().prefWidthProperty().unbind();
        useAPIPane.getClassPane().prefHeightProperty().unbind();
        useAPIPane.getClassPane().setPrefWidth(100);
        useAPIPane.getClassPane().setPrefHeight(40);
        useAPIPane.setPrefWidth(100);
        useAPIPane.setPrefHeight(40);
        
        useAPIPane.setLayoutX( ((selectedPane.getLayoutX()) + (selectedPane.getPrefWidth())) + useAPIPane.getPrefWidth()/2);
        
        
        
        line.endXProperty().bind(useAPIPane.layoutXProperty().add(useAPIPane.getClassPane().prefWidthProperty().divide(2)));
        line.endYProperty().bind(useAPIPane.layoutYProperty().add(useAPIPane.getClassPane().prefHeightProperty()));
        
        
        if (alreadyInDesign == false) {
        workspace.getDesignPane().getChildren().add(useAPIPane);
        workspace.getDesignPane().getChildren().add(line);
        }
        }
        
    }
    
       public void processSnapshot() {
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	ScrollPane canvas = workspace.getScrollPane();
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./snap"));
        fc.setTitle("Select Snapshopt location");
        File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", selectedFile);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
    }
       
}
