/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author Albert
 */
public class JcdClassPane extends VBox {

    private double x = 0;
    private double y = 0;
    private double mousex = 0;
    private double mousey = 0;
    private Line parentLine;

    private ArrayList<Line> interfaceLines;
    private ArrayList<Line> aggregateLines;
    private ArrayList<Line> usesLines;
    private ArrayList<String> aggregates;
    private ArrayList<String> uses;
    private Polygon usesConnector;
    private Rectangle aggregationConnecter;
    private Polygon inheritanceconnector;
    private JcdClassPane parentClassPane;
    private JcdClassPane childClassPane;
    private ArrayList<JcdClassPane> interfacePanes;
    private ArrayList<JcdClassPane> aggregatePanes;
    private ArrayList<JcdClassPane> usesPanes;

    public boolean external;

    public ArrayList<Line> getInterfaceLines() {
        return interfaceLines;
    }

    public void setInterfaceLines(ArrayList<Line> interfaceLines) {
        this.interfaceLines = interfaceLines;
    }

    public ArrayList<Line> getAggregateLines() {
        return aggregateLines;
    }

    public void setAggregateLines(ArrayList<Line> aggregateLines) {
        this.aggregateLines = aggregateLines;
    }

    public ArrayList<Line> getUsesLines() {
        return usesLines;
    }

    public void setUsesLines(ArrayList<Line> usesLines) {
        this.usesLines = usesLines;
    }

    public ArrayList<String> getAggregates() {
        return aggregates;
    }

    public ArrayList<String> getUses() {
        return uses;
    }

    public JcdClassPane getParentClassPane() {
        return parentClassPane;
    }

    public void setParentClassPane(JcdClassPane parentClassPane) {
        this.parentClassPane = parentClassPane;
    }

    public Line getParentLine() {
        return parentLine;
    }

    public void setParentLine(Line parentLine) {
        this.parentLine = parentLine;
    }

    public JcdClassPane getChildClassPane() {
        return childClassPane;
    }

    public void setChildClassPane(JcdClassPane childClassPane) {
        this.childClassPane = childClassPane;
    }

    public ArrayList<JcdClassPane> getInterfacePanes() {
        return interfacePanes;
    }

    public void setInterfacePanes(ArrayList<JcdClassPane> interfacePanes) {
        this.interfacePanes = interfacePanes;
    }

    public ArrayList<JcdClassPane> getAggregatePanes() {
        return aggregatePanes;
    }

    public void setAggregatePanes(ArrayList<JcdClassPane> aggregatePanes) {
        this.aggregatePanes = aggregatePanes;
    }

    public ArrayList<JcdClassPane> getUsesPanes() {
        return usesPanes;
    }

    public void setUsesPanes(ArrayList<JcdClassPane> usesPanes) {
        this.usesPanes = usesPanes;
    }

    public Rectangle getAggregationConnecter() {
        return aggregationConnecter;
    }

    public void setAggregationConnecter(Rectangle aggregationConnecter) {
        this.aggregationConnecter = aggregationConnecter;
    }

    public Polygon getInheritanceconnector() {
        return inheritanceconnector;
    }

    public void setInheritanceconnector(Polygon inheritanceconnector) {
        this.inheritanceconnector = inheritanceconnector;
    }

    public Polygon getUsesConnector() {
        return usesConnector;
    }

    public void setUsesConnector(Polygon usesConnector) {
        this.usesConnector = usesConnector;
    }

    public boolean isExternal() {
        return external;
    }

    public void setExternal(boolean isExternal) {
        external = isExternal;
    }

    VBox classPackage;
    VBox classNameType;
    VBox classVariables;
    VBox classMethods;

    Text classPackageArea;
    Text classNameArea;
    Text classVariablesArea;
    Text classMethodsArea;

    JcdClass associatedClass;

    ArrayList<Double> paneData;

    public JcdClassPane() {

        setPrefHeight(150);
        setPrefWidth(250);
        //setMaxWidth(400);

        external = false;
        interfaceLines = new ArrayList();
        interfacePanes = new ArrayList();
        aggregateLines = new ArrayList();
        aggregatePanes = new ArrayList();
        aggregates = new ArrayList();
        usesLines = new ArrayList();
        usesPanes = new ArrayList();
        uses = new ArrayList();
        classPackage = new VBox();
        classPackage.setPrefSize(20, 80);
        classPackage.setPrefHeight(20);
        classPackage.setPrefWidth(80);
        classPackage.setMaxWidth(400 / 5);

        //classPackage.prefWidthProperty().bind(this.prefWidthProperty().divide(5));
        classNameType = new VBox();
        classNameType.setPrefSize(70, 250);
        classVariables = new VBox();
        classVariables.setPrefSize(70, 250);
        classMethods = new VBox();
        classMethods.setPrefSize(70, 250);

        this.getChildren().add(classPackage);
        classPackageArea = new Text();
        //classPackageArea.setWrappingWidth(classPackage.getPrefWidth());
        classPackageArea.wrappingWidthProperty().bind(classPackage.prefWidthProperty());
        classPackage.getChildren().add(classPackageArea);
        this.getChildren().add(classNameType);
        //classNameType.setMaxWidth(400);
        classNameArea = new Text();
        classNameType.getChildren().add(classNameArea);
        //classNameArea.setWrappingWidth(this.getPrefWidth());
        classNameArea.wrappingWidthProperty().bind(this.prefWidthProperty());
        this.getChildren().add(classVariables);
        classVariablesArea = new Text();
        //classVariablesArea.setWrappingWidth(this.getPrefWidth());
        classVariablesArea.wrappingWidthProperty().bind(this.prefWidthProperty());
        classVariables.getChildren().add(classVariablesArea);
        this.getChildren().add(classMethods);
        classMethodsArea = new Text();
        classMethodsArea.wrappingWidthProperty().bind(this.prefWidthProperty());
        classMethods.getChildren().add(classMethodsArea);

        setUpStyle();

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getMouseX() {
        return mousex;
    }

    public double getMouseY() {
        return mousey;
    }

    public void setX(double nx) {
        x = nx;
    }

    public void setY(double ny) {
        y = ny;
    }

    public void setMouseX(double nx) {
        mousex = nx;
    }

    public void setMouseY(double ny) {
        mousey = ny;
    }

    public VBox getClassPane() {
        return classPackage;
    }

    public VBox getPackagePane() {
        return classPackage;
    }

    public VBox getVariablesPane() {
        return classVariables;
    }

    public VBox getMethodsPane() {
        return classMethods;
    }

    public Text getPackageTA() {
        return classPackageArea;
    }

    public Text getClassTA() {
        return classNameArea;
    }

    public Text getVariablesTA() {
        return classVariablesArea;
    }

    public Text getMethodsTA() {
        return classMethodsArea;
    }

    public JcdClass getAssociatedClass() {
        return associatedClass;
    }

    public void setAssociatedClass(JcdClass newClass) {
        associatedClass = newClass;
    }

    public void setUpStyle() {
        classPackage.getStyleClass().add("paneTextAreasCP");
        classNameType.getStyleClass().add("paneTextAreasCN");
        classVariables.getStyleClass().add("paneTextAreasV");
        classMethods.getStyleClass().add("paneTextAreasM");
    }

    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        setLayoutX(initX);
        setLayoutY(initY);
        this.setWidth(initWidth);
        this.setHeight(initHeight);
    }

}
