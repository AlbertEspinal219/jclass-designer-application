package jcd.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import jcd.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES

    // CURRENT STATE OF THE APP
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;

    JcdClass selectedClass;
    ArrayList<JcdClass> classes;
    ObservableList<Node> classPanes;
    ArrayList<JcdClassPane> externalPanes;
    ArrayList<String> classNames;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
        // KEEP THE APP FOR LATER
        app = initApp;
        selectedClass = null;
        classes = new ArrayList();
        classNames = new ArrayList();
        externalPanes = new ArrayList();
        // classesClone = new ArrayList();
        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(15);
        highlightedEffect = dropShadowEffect;

    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getDesignPane().getChildren().clear();
        workspace.resetWorkspace();
        selectedClass = null;
        classes.clear();
        classPanes.clear();

    }
    public void unhighlightClass(VBox classPane) {
        classPane.setEffect(null);
    }

    public void highlightClass(VBox classPane) {
        classPane.setEffect(highlightedEffect);
    }

    public ArrayList<JcdClass> getClasses() {
        return classes;
    }

    public ObservableList<Node> getClassPanes() {
        return classPanes;
    }
    
    public ArrayList<JcdClassPane> getExternalPanes() {
        return externalPanes;
    }
    
    public void addExternalPane(JcdClassPane pane) {
        externalPanes.add(pane);
    }

    public ArrayList<String> getClassNames() {
        return classNames;
    }

    public void setClassNames(ArrayList<String> newClassNames) {
        classNames = newClassNames;
    }

    public void setClassPanes(ObservableList<Node> initPanes) {
        classPanes = initPanes;

    }

    public void addClassPane(JcdClassPane newClassPane) {
        classPanes.add(newClassPane);
    }

    public JcdClass getselectedClass() {
        return selectedClass;
    }

    public void setSelectedClass(JcdClass toBeSelected) {
        selectedClass = toBeSelected;
    }
}
