/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Albert
 */
public class Method {

    private String name;
    static int nameCount;
    private String returnType;
    private boolean staticMethod;
    private boolean abstractMethod;
    private String access;

    private SimpleStringProperty pname;
    private SimpleStringProperty preturnType;
    private SimpleBooleanProperty pstaticMethod;
    private SimpleBooleanProperty pabstractMethod;
    private SimpleStringProperty paccess;

    private SimpleStringProperty parg1Name;
    private SimpleStringProperty parg1Type;

    private SimpleStringProperty parg2Name;
    private SimpleStringProperty parg2Type;
    private SimpleStringProperty parg3Name;
    private SimpleStringProperty parg3Type;
    private SimpleStringProperty parg4Name;
    private SimpleStringProperty parg4Type;
    private SimpleStringProperty parg5Name;
    private SimpleStringProperty parg5Type;
    private SimpleStringProperty parg6Name;
    private SimpleStringProperty parg6Type;
    private SimpleStringProperty parg7Name;
    private SimpleStringProperty parg7Type;
    private SimpleStringProperty parg8Name;
    private SimpleStringProperty parg8Type;
    private SimpleStringProperty parg9Name;
    private SimpleStringProperty parg9Type;
    private SimpleStringProperty parg10Name;
    private SimpleStringProperty parg10Type;

    private ArrayList<Argument> args;

    public Method() {
        pname = new SimpleStringProperty();
        preturnType = new SimpleStringProperty();
        pstaticMethod = new SimpleBooleanProperty();
        pabstractMethod = new SimpleBooleanProperty();
        paccess = new SimpleStringProperty();
        pstaticMethod.addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

                staticMethod = t1;
                pstaticMethod.set(t1);

            }
        });

        pabstractMethod.addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

                abstractMethod = t1;
                pabstractMethod.set(t1);

            }
        });
        args = new ArrayList();
    }

    public Method(String mName, String mArg1Name, String mArg1Type,
            String mreturnType, String maccess, boolean isAbstract, boolean isStatic) {
        nameCount++;
        name = mName + nameCount;
        returnType = mreturnType;
        abstractMethod = isAbstract;
        access = maccess;
        staticMethod = isStatic;
        args = new ArrayList();
        if (mArg1Name != null && mArg1Type != null) {
            args.add(new Argument(mArg1Name, mArg1Type));
        }

        pname = new SimpleStringProperty(mName);
        preturnType = new SimpleStringProperty(mreturnType);
        pstaticMethod = new SimpleBooleanProperty(isStatic);
        pabstractMethod = new SimpleBooleanProperty(isAbstract);
        paccess = new SimpleStringProperty(maccess);

        pstaticMethod.addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

                staticMethod = t1;
                pstaticMethod.set(t1);

            }
        });

        pabstractMethod.addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

                abstractMethod = t1;
                pabstractMethod.set(t1);

            }
        });
    }

    public String getName() {
        return name;
    }

    public String getReturnType() {
        return returnType;
    }

    public boolean isStatic() {
        return staticMethod;
    }

    public boolean isAbstract() {
        return abstractMethod;
    }

    public String getAccess() {
        return access;
    }

    public void setName(String argName) {
        name = argName;
        //pname.set(argName);
    }

    public void initializePName() {
        nameCount++;
        name = name + nameCount;
        pname.set(name);
    }

    public void initalizePReturnType() {
        preturnType.set(returnType);
    }

    public void initializePAccess() {
        paccess.set(access);
    }

    public void initalizePAbstractMethod() {
        pabstractMethod.set(abstractMethod);
    }

    public void initalizePStaticMethod() {
        pstaticMethod.set(staticMethod);
    }

    public void initalizePArgs() {
        if (args.size() >= 1) {
            parg1Name = new SimpleStringProperty(args.get(0).getArgName());
            parg1Type = (SimpleStringProperty) args.get(0).pargTypeProperty();
        }
        if (args.size() >= 2) {
            parg2Name = new SimpleStringProperty(args.get(1).getArgName());
            parg2Type = (SimpleStringProperty) args.get(1).pargTypeProperty();
        }
        if (args.size() >= 3) {
            parg3Name = new SimpleStringProperty(args.get(2).getArgName());
            parg3Type = (SimpleStringProperty) args.get(2).pargTypeProperty();
        }
        if (args.size() >= 4) {
            parg4Name = new SimpleStringProperty(args.get(3).getArgName());
            parg4Type = new SimpleStringProperty(args.get(3).getArgType());
        }
        if (args.size() >= 5) {
            parg5Name = new SimpleStringProperty(args.get(4).getArgName());
            parg5Type = new SimpleStringProperty(args.get(4).getArgType());
        }
        if (args.size() >= 6) {
            parg6Name = new SimpleStringProperty(args.get(5).getArgName());
            parg6Type = new SimpleStringProperty(args.get(5).getArgType());
        }
        if (args.size() >= 7) {
            parg7Name = new SimpleStringProperty(args.get(6).getArgName());
            parg7Type = new SimpleStringProperty(args.get(6).getArgType());
        }
        if (args.size() >= 8) {
            parg8Name = new SimpleStringProperty(args.get(7).getArgName());
            parg8Type = new SimpleStringProperty(args.get(7).getArgType());
        }
        if (args.size() >= 9) {
            parg9Name = new SimpleStringProperty(args.get(8).getArgName());
            parg9Type = new SimpleStringProperty(args.get(8).getArgType());
        }
        if (args.size() == 10) {
            parg10Name = new SimpleStringProperty(args.get(9).getArgName());
            parg10Type = new SimpleStringProperty(args.get(9).getArgType());
        }
    }

    public void setReturn(String nreturnType) {
        returnType = nreturnType;
        //preturnType.set(nreturnType);
    }

    public void setAccess(String nAccess) {
        access = nAccess;
        //paccess.set(nAccess);
    }

    public boolean isStaticMethod() {
        return staticMethod;
    }

    public void setStaticMethod(boolean staticMethod) {
        this.staticMethod = staticMethod;
        //pstaticMethod.set(staticMethod);
    }

    public boolean isAbstractMethod() {
        return abstractMethod;
    }

    public void setAbstractMethod(boolean abstractMethod) {
        this.abstractMethod = abstractMethod;
        //pabstractMethod.set(abstractMethod);
    }

    public StringProperty pnameProperty() {
        return pname;
    }

    public void setPName(String newName) {
        pname.set(newName);
        name = newName;
    }

    public StringProperty preturnTypeProperty() {
        return preturnType;
    }

    public void setPReturnType(String newReturn) {
        preturnType.set(newReturn);
        returnType = newReturn;
    }

    public BooleanProperty pstaticMethodProperty() {
        return pstaticMethod;
    }

    public void setPStaticMethod(boolean newStatic) {
        pstaticMethod.set(newStatic);
        staticMethod = newStatic;
    }

    public BooleanProperty pabstractMethodProperty() {
        return pabstractMethod;
    }

    public void setPAbstractMethod(boolean newAbstract) {
        pabstractMethod.set(newAbstract);
        abstractMethod = newAbstract;
    }

    public StringProperty paccessProperty() {
        return paccess;
    }

    public void setPAccess(String newAccess) {
        paccess.set(newAccess);
        access = newAccess;
    }

    public StringProperty parg1NameProperty() {
        return parg1Name;
    }

    public StringProperty parg1TypeProperty() {
        if (args.size() > 0) {
            return args.get(0).pargTypeProperty();
        } else {
            return null;
        }
    }

    public StringProperty parg2NameProperty() {
        return parg2Name;
    }

    public StringProperty parg2TypeProperty() {
        if (args.size() > 1) {
            return args.get(1).pargTypeProperty();
        } else {
            return null;
        }
    }

    public StringProperty parg3NameProperty() {
        return parg3Name;
    }

    public StringProperty parg3TypeProperty() {
        return args.get(2).pargTypeProperty();
    }

    public StringProperty arg4NameProperty() {
        return parg4Name;
    }

    public StringProperty parg4TypeProperty() {
        return args.get(3).pargTypeProperty();
    }

    public StringProperty parg5NameProperty() {
        return parg5Name;
    }

    public StringProperty parg5TypeProperty() {
        return args.get(4).pargTypeProperty();
    }

    public StringProperty parg6NameProperty() {
        return parg6Name;
    }

    public StringProperty parg6TypeProperty() {
        return args.get(5).pargTypeProperty();
    }

    public StringProperty parg7NameProperty() {
        return parg7Name;
    }

    public StringProperty parg7TypeProperty() {
        return args.get(6).pargTypeProperty();
    }

    public StringProperty parg8NameProperty() {
        return parg8Name;
    }

    public StringProperty parg8TypeProperty() {
        return args.get(7).pargTypeProperty();
    }

    public StringProperty parg9NameProperty() {
        return parg9Name;
    }

    public StringProperty parg9TypeProperty() {
        return args.get(8).pargTypeProperty();
    }

    public StringProperty parg10NameProperty() {
        return parg10Name;
    }

    public StringProperty parg10TypeProperty() {
        return args.get(9).pargTypeProperty();
    }

    public void setparg1TypeProperty(String newVal) {
        args.get(0).setpArgType(newVal);
        //parg1Type.set(newVal);
    }

    public void setparg2TypeProperty(String newVal) {
        args.get(1).setpArgType(newVal);
    }

    public void setparg3TypeProperty(String newVal) {
        args.get(2).setpArgType(newVal);
    }

    public void setparg4TypeProperty(String newVal) {
        args.get(3).setpArgType(newVal);
    }

    public void setparg5TypeProperty(String newVal) {
        args.get(4).setpArgType(newVal);
    }

    public void setparg6TypeProperty(String newVal) {
        args.get(5).setpArgType(newVal);
    }

    public void setparg7TypeProperty(String newVal) {
        args.get(6).setpArgType(newVal);
    }

    public void setparg8TypeProperty(String newVal) {
        args.get(7).setpArgType(newVal);
    }

    public void setparg9TypeProperty(String newVal) {
        args.get(8).setpArgType(newVal);
    }

    public void setparg10TypeProperty(String newVal) {
        args.get(9).setpArgType(newVal);
    }

    public void addArg(String argName, String argType) {
        args.add(new Argument(argName, argType));
    }

    public void addArg(Argument newArg) {
        args.add(newArg);
    }

    public Argument getArg(int pos) {
        return args.get(pos);
    }

    public ArrayList<Argument> getArguments() {
        return args;
    }

    public String toString() {
        String methodText = "";

        if (access.equals("public")) {
            methodText += "+";
        }
        if (access.equals("private")) {
            methodText += "-";
        }
        if (staticMethod) {
            methodText += "$";
        }

        methodText += name;

        methodText += "(";

        for (int i = 0; i < args.size(); i++) {
            methodText += args.get(i).getArgName();
            methodText += " : ";
            methodText += args.get(i).getArgType();
            if (i + 1 != args.size()) {
                if (args.get(i + 1).getArgType().equals("")) {
                    break;
                }
            }
            if (i == 9) {
                break;
            }
            methodText += ", ";
        }

        methodText += ")";

        methodText += ":";
        methodText += returnType;

        return methodText;
    }

    public String exportString(String className) {
        ArrayList<String> primitives = new ArrayList();
        primitives.add("byte");
        primitives.add("short");
        primitives.add("int");
        primitives.add("long");
        primitives.add("float");
        primitives.add("double");
        primitives.add("boolean");
        primitives.add("char");

        String exportMeth = "";

        exportMeth += access + " ";

        if (staticMethod) {
            exportMeth += "static" + " ";
        }

        exportMeth += returnType + " ";

        exportMeth += name + "(";

        for (int i = 0; i < args.size(); i++) {
            if ((args.get(i) != null) && !(args.get(i).equals(""))) {
                if ((args.get(i).getArgType() != null) && !(args.get(i).getArgType().equals(""))) {
                    exportMeth += args.get(i).getArgType() + " " + args.get(i).getArgName() + ",";
                }
            }
        }
        if (args.size() > 1) {
            if ((args.get(0).getArgType() != null) && !(args.get(0).getArgType().equals(""))) {
                exportMeth = exportMeth.substring(0, exportMeth.lastIndexOf(","));
            }
        }

        exportMeth += ")";

        exportMeth += " {";
        if (!(name.trim().equals(className.trim()))) {
            if (!(returnType.equals("void"))) {
                exportMeth += "\n";
                exportMeth += "\t\t";
                //Variable dummyVar = new Variable("dummyVal",returnType,"",false);

                //String dummyReturn = dummyVar.exportString();
                //dummyReturn = dummyReturn.trim();
                //exportMeth += dummyReturn;
                //exportMeth += "\n";
                //exportMeth += "\t\t";
                boolean notPrimitive = true;
                for (String val : primitives) {
                    if (returnType.equals(val)) {
                        notPrimitive = false;
                    }
                }

                if (notPrimitive) {
                    //String capitalType = dummyVar.getType();
                    //capitalType = capitalType.substring(0, 1).toUpperCase() + capitalType.substring(1);
                    exportMeth += "return null;";
                } else {
                    switch (returnType) {
                        case "byte":
                            //exportMeth+= dummyVar.getName() + " = 0;";
                            exportMeth += "return 0;";
                            break;
                        case "short":
                            //exportMeth+= dummyVar.getName() + " = 0;";
                            exportMeth += "return 0;";
                            break;
                        case "int":
                            //exportMeth+= dummyVar.getName() + " = 0;";
                            exportMeth += "return 0;";
                            break;
                        case "long":
                            //exportMeth+= dummyVar.getName() + " = 0.0;";
                            exportMeth += "return 0;";
                            break;
                        case "double":
                            //exportMeth+= dummyVar.getName() + " = 0.0;";
                            exportMeth += "return 0;";
                            break;
                        case "boolean":
                            exportMeth += "return false;";
                            break;
                        case "char":
                            exportMeth += exportMeth += "return 'a';";
                            break;

                    }
                }

                //exportMeth+= "\n";
                // exportMeth+= "\t\t";
                //exportMeth += "return " + dummyVar.getName() + ";";
                exportMeth += "\n";
                exportMeth += "\t";
            }
        }

        exportMeth += "}";

        return exportMeth;
    }
}
