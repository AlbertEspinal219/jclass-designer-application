/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import saf.AppTemplate;

/**
 *
 * @author Albert
 */
public class CheckBoxTableCelll<S, T> extends TableCell<S, T> {

    private final CheckBox checkBox;
    private ObservableValue<T> ov;
    public AppTemplate app;

    public CheckBoxTableCelll(AppTemplate initApp) {

        this.checkBox = new CheckBox();

        this.checkBox.setAlignment(Pos.CENTER);

        app = initApp;

        setAlignment(Pos.CENTER);

        setGraphic(checkBox);

        checkBox.setOnMouseReleased(e -> {
            DataManager dataManager = (DataManager) app.getDataComponent();
            dataManager.getselectedClass().setPaneText();

        });

    }

    @Override
    public void updateItem(T item, boolean empty) {

        super.updateItem(item, empty);

        if (empty) {

            setText(null);

            setGraphic(null);

        } else {

            setGraphic(checkBox);

            if (ov instanceof BooleanProperty) {

                checkBox.selectedProperty().unbindBidirectional((BooleanProperty) ov);

            }

            ov = getTableColumn().getCellObservableValue(getIndex());

            if (ov instanceof BooleanProperty) {

                checkBox.selectedProperty().bindBidirectional((BooleanProperty) ov);

            }

        }

    }
}
