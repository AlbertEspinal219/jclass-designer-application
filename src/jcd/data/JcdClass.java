package jcd.data;

import java.util.ArrayList;

/**
 *
 * @author Albert
 */
public class JcdClass {

    private String parentClass;
    private String childClass;
    private String className;
    private String packageName;
    private String externalParent;
    private static int count;
    private ArrayList<String> interfaces;

    public ArrayList<String> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(ArrayList<String> interfaces) {
        this.interfaces = interfaces;
    }
    private boolean isInterface;
    private boolean isAbstract;
    private boolean external;

    public boolean isIsInterface() {
        return isInterface;
    }

    public void setIsInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    public boolean isIsAbstract() {
        return isAbstract;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public String getClassPath() {
        return classPath;
    }

    public boolean isExternal() {
        return external;
    }

    public void setExternal(boolean newExternal) {
        external = newExternal;
    }

    public String getExternalParent() {
        return externalParent;
    }

    public void setExternalParent(String nexternalParent) {
        externalParent = nexternalParent;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }
    private String classPath;
    public ArrayList<Variable> variables;
    public ArrayList<Method> methods;
    private JcdClassPane jcdClassPane;

    public JcdClass() {
        count++;
        className = "Dummy" + count;
        packageName = "";
        variables = new ArrayList();
        methods = new ArrayList();
        parentClass = "";
        childClass = "";
        externalParent = "";
        isInterface = false;
        isAbstract = false;
        interfaces = new ArrayList();
    }

    public ArrayList<Variable> getVariables() {
        return variables;
    }

    public ArrayList<Method> getMethods() {
        return methods;
    }

    public void setVariables(ArrayList<Variable> variabless) {
        variables = variabless;
    }

    public void setMethods(ArrayList<Method> methodss) {
        methods = methodss;
    }

    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setClassName(String newClassName) {
        className = newClassName;
    }

    public void setPackageName(String newPackageName) {
        packageName = newPackageName;
    }

    public String getParent() {
        return parentClass;
    }

    public String getChild() {
        return childClass;
    }

    public void setParent(String newParent) {
        parentClass = newParent;
    }

    public void setChild(String newChild) {
        childClass = newChild;
    }

    public void setClassPane(JcdClassPane classPane) {
        jcdClassPane = classPane;
    }

    public JcdClassPane getClassPane() {
        return jcdClassPane;
    }

    public void setPaneText() {
        JcdClassPane classPane = jcdClassPane;
        ArrayList<Variable> vars = getVariables();
        ArrayList<Method> methds = getMethods();

        String varText = "";

        for (Variable v : vars) {
            varText += v.toString();
            varText += "\n";
        }
        String methodText = "";

        for (Method m : methds) {
            methodText += m.toString();
            if (m.isAbstract()) {
                methodText+= "{abstract}";
            }
            methodText += "\n";
        }

        classPane.getVariablesTA().setText(varText);
        classPane.getMethodsTA().setText(methodText);

    }

}
