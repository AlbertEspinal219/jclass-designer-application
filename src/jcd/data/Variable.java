/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Albert
 */
public class Variable {
    
    private String name;
    static int nameCount;
    private String type;
    private String accessibility;
    private final SimpleStringProperty pname;
    private final SimpleStringProperty ptype;
    private final SimpleStringProperty paccessibility;
    private boolean staticVal;

    //public String getpname() {
    //  return pname.get();
    //}
    //public String getptype() {
    //   return ptype.get();
    //}
    // public String getpaccessibility() {
    //    return paccessibility.get();
    ///}
    public StringProperty pnameProperty() {
        return pname;
    }

    public StringProperty ptypeProperty() {
        return ptype;
    }

    public StringProperty paccessibilityProperty() {
        return paccessibility;
    }

    public BooleanProperty staticCbValueProperty() {
        return staticCbValue;
    }

    public void setPName(String pnamee) {
        pname.set(pnamee);
        name = pnamee;
    }

    public void setPType(String ptypee) {
        ptype.set(ptypee);
        type = ptypee;
    }

    public void setPAccessibility(String paccessibilityy) {
        paccessibility.set(paccessibilityy);
        accessibility = paccessibilityy;
    }

    //public boolean getStaticCbValue() {
    //  return staticCbValue.get();
    //}
    //public void setStaticCbValue(boolean staticVal) {
    //  this.staticCbValue.set(staticVal);
    //}
    private SimpleBooleanProperty staticCbValue;

    /**
     * public Variable() { name = "my_var"; type = "int"; accessibility =
     * "public"; staticVal = false; }
     */

    public Variable(String vname, String vtype, String vaccessibility, boolean isstaticVal) {
        nameCount++;
        name = vname + nameCount;
        type = vtype;
        accessibility = vaccessibility;
        pname = new SimpleStringProperty(name);
        ptype = new SimpleStringProperty(vtype);
        paccessibility = new SimpleStringProperty(vaccessibility);
        staticCbValue = new SimpleBooleanProperty(isstaticVal);
        staticVal = isstaticVal;

        staticCbValue.addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {

                staticVal = t1;
                staticCbValue.set(t1);

            }
        });

    }
    //staticCbValue.set(isstaticVal);

    public boolean isStatic() {
        return staticVal;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getAccessibility() {
        return accessibility;
    }

    public void setStatic(boolean newStaticVal) {
        staticVal = newStaticVal;
    }

    public void setName(String newName) {
        name = newName;
    }

    public void setType(String newType) {
        type = newType;
    }

    public void setAccessibility(String newAccess) {
        accessibility = newAccess;
    }

    @Override
    public String toString() {
        String toString = "";

        if (accessibility.equals("public")) {
            toString += "+";
        }
        if (accessibility.equals("private")) {
            toString += "-";
        }
        if (accessibility.equals("protected")) {
            toString += "#";
        }
        if (staticVal) {
            toString += "$";
        }

        toString += name + " : " + type;
        return toString;
    }

    public String exportString() {
        String exportVar = "";

        exportVar += accessibility + " ";

        if (this.staticVal) {
            exportVar += "static ";
        }

        exportVar += type + " ";

        exportVar += name + ";";

        return exportVar;
    }
}
