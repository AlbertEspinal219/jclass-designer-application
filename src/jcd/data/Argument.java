/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Albert
 */
public class Argument {

    private String argName;
    private String argType;

    private SimpleStringProperty pargName;
    private SimpleStringProperty pargType;
    static int counter;

    public Argument(String nargName, String nargType) {
        counter++;
        argName = "myMeth" + counter;
        argType = nargType;
        pargName = new SimpleStringProperty(argName);
        pargType = new SimpleStringProperty(argType);
        if (counter == 10) 
            counter = 0;
    }

    public Argument(SimpleStringProperty nArgName, SimpleStringProperty nArgType) {
        pargName = nArgName;
        pargType = nArgType;
        argName = pargName.get();
        argType = pargType.get();
    }

    public String getArgName() {
        return pargName.get();
    }

    public void setArgName(String argName) {
        this.argName = argName;
    }

    public String getArgType() {
        return pargType.get();
    }

    public void setArgType(String argType) {
        this.argType = argType;
    }

    public StringProperty pargNameProperty() {
        return pargName;
    }

    public StringProperty pargTypeProperty() {
        return pargType;
    }

    public void setpArgName(String newArgName) {
        pargName.set(newArgName);
        argName = newArgName;
    }

    public void setpArgType(String newArgType) {
        argType = newArgType;
        pargType.set(newArgType);
    }

}
